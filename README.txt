==================== testscript ====================
Shell script criado para testes automáticos com o material 
disponibilizado nas etapas 2 e 3 (mais os iksAlgorithms).

Executar ./test.sh na pasta /testscript.

Executa Cmake, make e o programa para todas as entradas nas pastas.

Para entradas verdadeiras que não possuem a resposta oficial, 
colocar na pasta /testfiles/nocomparable/right. Para entradas
falsas, colocar o arquivo em /testfiles/nocomparable/wrong.

Para entradas com solução disponibilizada (grafo), pode-se 
colocar na pasta /testfiles/comparable/test, e o gabarito em
 /testfiles/comparable/true/


==================== iksAlgorithms ====================

Algoritmos desenvolvidos se encontram na pasta /iksAlgorithms:
1 - Cálculo do PI
2 - Cálculo de e (euler)

O parser está configurado para criar o arquivo "resultGraph.dot" 
como resultado da análise. Esse arquivo pode ser convertido
 para .png com o comando:

dot resultGraph.dot -Tpng -o resultGraph.png

Na pasta /iksAlgorithms já se encontram os grafos para os algoritmos
desenvolvidos.

