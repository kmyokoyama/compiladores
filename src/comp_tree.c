#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_tree.h"
#include "gv.h"
#include "parser.h"
#include "iks_ast.h"
#include "semantic_analyser.h"

#define GV_FUNCTION

/* ========== Internal functions declaration ========== */
/**
*	Create a node to be used in ASTs.
*/
COMP_TREE_T* create_tree_node(int nodeType, COMP_DICT_ITEM_T* dictPointer, char* name);

/* ========== External functions implementation ========== */

COMP_TREE_T* create_ast_node(int nodeType, COMP_DICT_ITEM_T* dictPointer, char* name, int numSons, ... ){

	int i;
	COMP_TREE_T* newNode;

	va_list sonPointers;

	/* Initialize argument list */
	va_start(sonPointers, numSons);

	/* Create the new node */
	newNode = create_tree_node(nodeType, dictPointer, name);

	/* Add the sons */
	for(i = 0; i < numSons; i++){ 
		newNode->son_list[i] = va_arg(sonPointers, COMP_TREE_T* );
	}

	//newNode->nodeSize = calculateNodeSize();

	va_end(sonPointers);
	
	switch(newNode->nodeType)
	{
		case IKS_AST_INPUT:
							for(i=0;i<numSons;i++)
							{
								/* Argument for input command is not a valid expression */
								if(newNode->son_list[i]->nodeType != IKS_AST_IDENTIFICADOR)
								{
									exit(IKS_ERROR_WRONG_PAR_INPUT);
								}
								/* Don't match any from IKS_SIMBOLO_INT(1) to IKS_SIMBOLO_BOOL(5) */
								else if(newNode->son_list[i]->dictPointer->itemType < 1
										|| newNode->son_list[i]->dictPointer->itemType > 5)
								{
									exit(IKS_ERROR_WRONG_PAR_INPUT);
								}
							} break;
							
		case IKS_AST_OUTPUT:
							for(i=0;i<numSons;i++)
							{
								/* Argument for output command is neither literal nor arithmetic expression */
								if( (newNode->son_list[i]->dictPointer->itemType != TK_LIT_STRING)
									|| (newNode->son_list[i]->nodeType != IKS_AST_ARIM_SOMA)
									|| (newNode->son_list[i]->nodeType != IKS_AST_ARIM_SUBTRACAO)
									|| (newNode->son_list[i]->nodeType != IKS_AST_ARIM_MULTIPLICACAO)
									|| (newNode->son_list[i]->nodeType != IKS_AST_ARIM_DIVISAO) )
								{
									exit(IKS_ERROR_WRONG_PAR_OUTPUT);
								}
							} break;
	}

	return newNode;
}


COMP_TREE_T* add_son_ast_node(COMP_TREE_T* target_node, int numSons, ... ){

	int i;
	va_list sonPointers;

	if(target_node == NULL)
		return NULL;

	/* Initialize argument list */
	va_start(sonPointers, numSons);

	/* Find a empty node and add the new ponter*/
	for(i = 0; i < MAX_TREE_SONS; i++){
		if(target_node->son_list[i] == NULL && numSons > 0){
			
			target_node->son_list[i] = va_arg(sonPointers, COMP_TREE_T* );

			numSons--;
		}
	}

	va_end(sonPointers);

	return target_node;
}

void draw_tree(COMP_TREE_T* root_node){

	int i;

	if(root_node == NULL)
		return ;

	/* Declare this node*/
	gv_declare (root_node->nodeType, root_node, root_node->name);

	/* Link with sons */
	for(i = 0; i < MAX_TREE_SONS; i++){
		if(root_node->son_list[i] != NULL){
			gv_connect (root_node, root_node->son_list[i]);
			draw_tree(root_node->son_list[i]);
		}
	}
}

/* ========== Internal functions implementation ========== */
COMP_TREE_T* create_tree_node(int nodeType, COMP_DICT_ITEM_T* dictPointer, char* name){
	
	COMP_TREE_T* newNode;
	int i;

	/* Allocate memory */
	newNode = malloc( sizeof(COMP_TREE_T) );

	/* Define its value */
	newNode->nodeType = nodeType;
	
	if(name != NULL){
		newNode->name = strdup(name);
	}
	else{
		newNode->name = NULL;
	}

	newNode->dictPointer = dictPointer;

	/* Clear pointer list */
	for(i = 0; i < MAX_TREE_SONS; i++){
		newNode->son_list[i] = NULL;
	}

	return newNode;

}
