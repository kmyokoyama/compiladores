#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "semantic_analyser.h"

#define SYNTAX_DEBUG


void setSizeDict(COMP_DICT_ITEM_T* newItem)
{
   int type;
   type = newItem->itemType;

   if(type==IKS_SIMBOLO_INT)
     newItem->size=4;
   else if(type==IKS_SIMBOLO_FLOAT)
     newItem->size=8;
   else  if(type==IKS_SIMBOLO_CHAR)
     newItem->size=1;
   else  if(type==IKS_SIMBOLO_STRING)
     newItem->size=strlen(newItem->key);
   else  if(type==IKS_SIMBOLO_BOOL)
     newItem->size=1;  
}

void verifyAttributionVector(COMP_TREE_T* node)
{

   int indexVector;
/////////////////////////////////////////////// get the vector index ////////////////////////////////////////////////////////////

   if(( node->son_list[0]->son_list[1]->nodeType >= 12)&&(node->son_list[0]->son_list[1]->nodeType <= 15))// verify if son1 is node +,-, ... (based on defines iks_ast.h)
   {
		indexVector=node->son_list[0]->son_list[1]->nodeSymbol;
   }
   else
   {
		indexVector=node->son_list[0]->son_list[1]->dictPointer->itemType; // get symbol (float, int, etc) from son1
   }

/////////////////////////////////////////// verify the vector index, that must be int ///////////////////////////////////////////

   //printf(" O indice do vetor e: %d",indexVector);

   if((indexVector==282)  || (indexVector==262) || (indexVector==IKS_SIMBOLO_STRING)) // if the index is a string => error
   {
      #ifdef SYNTAX_DEBUG
                        printf("ERRO: Indice do vetor nao pode ser string. - IKS_ERROR_STRING_TO_X\n");
                #endif
                exit(IKS_ERROR_STRING_TO_X);

   }

  if((indexVector==281)  || (indexVector==261) || (indexVector==IKS_SIMBOLO_CHAR)) // if the index is a string => error
   {
      #ifdef SYNTAX_DEBUG
                        printf("ERRO: Indice do vetor nao pode ser char. - IKS_ERROR_CHAR_TO_X\n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);

   }
}


void verifyAttribution(COMP_TREE_T* node)
{


   int son1,son2;


   if(( node->son_list[0]->nodeType >= 12)&&(node->son_list[0]->nodeType <= 15))// verify if son1 is node +,-, ... (based on defines iks_ast.h)
   {
		son1=node->son_list[0]->nodeSymbol;
   }
   else if(node->son_list[0]->nodeType==27) // verify if son 1 is a function call
   {
               son1 = node -> son_list[0]->son_list[0]->dictPointer->itemType;
               //printf("\n %d",son1);
   }
   else
   {
		son1=node->son_list[0]->dictPointer->itemType; // get symbol (float, int, etc) from son1
   }
   
   if((node->son_list[1]->nodeType >= 12)&&( node->son_list[1]->nodeType <= 15))// verify if son2 is node +,-, ...
   {	
		son2=node->son_list[1]->nodeSymbol;
   }
   else if(node->son_list[1]->nodeType==27) // verify if son 2 is a function call
   {
   
                 son2 = node -> son_list[1]->son_list[0]->dictPointer->itemType;
                 //printf("\n %d",son2);
   }
   else
   {
		son2=node->son_list[1]->dictPointer->itemType; //get symbol (float, int, etc) from son2
   }

///////////////////////////////////// regras de coerção (marcação no dicionário das variáveis que deverão sofrer coerção)//////////////////////////////////////
    
   if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_FLOAT) // coerção implícita de int para float
        || (son1==IKS_SIMBOLO_INT && son2==278))

  {    node->son_list[0]->dictPointer->cast = CAST_INT_TO_FLOAT;
       //printf(" int deve virar float : %d",node->son_list[0]->dictPointer->cast );

  }

   if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_BOOL) // coerção implícita de int para bool
        || (son1==IKS_SIMBOLO_INT && son2==260))

  {    node->son_list[0]->dictPointer->cast = CAST_INT_TO_BOOL;
       //printf(" int deve virar bool: %d",node->son_list[0]->dictPointer->cast );
       

  }

  if(	(son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_INT) // coerção implícita de float para int
        || (son1==IKS_SIMBOLO_FLOAT && son2==277))

  {    node->son_list[0]->dictPointer->cast = CAST_FLOAT_TO_INT;
       //printf(" float deve virar int: %d",node->son_list[0]->dictPointer->cast );;

  }

   if(	(son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_BOOL) // coerção implícita float para bool
        || (son1==IKS_SIMBOLO_FLOAT && son2==260))

  {    node->son_list[0]->dictPointer->cast = CAST_FLOAT_TO_BOOL;
       //printf(" float deve virar bool: %d",node->son_list[0]->dictPointer->cast );;

  }

  if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_INT) // coerção implícita bool para int
        || (son1==IKS_SIMBOLO_BOOL && son2==277))

  {    node->son_list[0]->dictPointer->cast = CAST_BOOL_TO_INT;
       //printf(" bool deve virar int: %d",node->son_list[0]->dictPointer->cast );;

  }
  
  if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_FLOAT) // coerção implícita bool para floatin
        || (son1==IKS_SIMBOLO_BOOL && son2==278))

  {    node->son_list[0]->dictPointer->cast = CAST_BOOL_TO_FLOAT;
       //printf(" bool deve virar float: %d",node->son_list[0]->dictPointer->cast );

  }


//////////////////////////////////// erros de coerção ///////////////////////////////////////


 if(	       (son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_INT)
		|| (son1==281 && son2==277)
		|| (son1==IKS_SIMBOLO_CHAR && son2==277))
   {		                                         // char and int => incompatible{
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE\n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }


  if(	        (son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_CHAR)
		|| (son1==277 && son2==281)
		|| (son1==IKS_SIMBOLO_INT && son2==281))
		                                          // char and int => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_CHAR_TO_X \n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

  if(	        (son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_CHAR)
		|| (son1==IKS_SIMBOLO_STRING && son2==281)
		|| (son1==282 && son2==IKS_SIMBOLO_CHAR)) // String = char => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_CHAR_TO_X \n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X );
   }

     if(	 
		(son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_CHAR && son2==282)
		|| (son1==281 && son2==IKS_SIMBOLO_STRING)) // char = string => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_STRING_TO_X  \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X );
   }


	if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_CHAR)
		|| (son1==IKS_SIMBOLO_BOOL && son2==281))   // bool = char  => incompatible 
  {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_CHAR_TO_X\n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

        if(	(son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_BOOL)
		|| (son1==281 && son2==IKS_SIMBOLO_BOOL)) // char = bool => wrong type
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE \n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }

   if(	        (son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_INT)
		|| (son1==282 && son2==277)
		|| (son1==282 && son2==IKS_SIMBOLO_INT)
		|| (son1==IKS_SIMBOLO_STRING && son2==277)) // string = int => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE \n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }

   if(	     (son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_STRING)
	     || (son1==277 && son2==IKS_SIMBOLO_STRING)
	     || (son1==IKS_SIMBOLO_INT && son2==282)) // int = string => error
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_STRING_TO_X  \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X );
   }
   
   if(	        (son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_STRING)
		|| (son1==278 && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==282)) // float = string => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_STRING_TO_X \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X);
   }

   if(		(son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==282 && son2==278)
		|| (son1==282 && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==IKS_SIMBOLO_STRING && son2==278)) // string = float => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE \n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }

   if(	        (son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_BOOL && son2==282)) // bool = string => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_STRING_TO_X \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X);
   }

  if(		(son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_BOOL)
		|| (son1==282 && IKS_SIMBOLO_BOOL))		// String = bool => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE \n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }

   
   if(	        (son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_CHAR)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==281)
		|| (son1==278 && son2==IKS_SIMBOLO_CHAR))		 // float = char => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_CHAR_TO_X \n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

  if(		(son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==IKS_SIMBOLO_CHAR && son2==278)
		|| (son1==281 && son2==IKS_SIMBOLO_FLOAT)) // char = float => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: IKS_ERROR_WRONG_TYPE \n");
                #endif
                exit(IKS_ERROR_WRONG_TYPE);
   }


}

void verifyArithOperation(COMP_TREE_T* node)
{
   int son1,son2;
   
   if(( node->son_list[0]->nodeType >= 12)&&(node->son_list[0]->nodeType <= 15))// verify if son1 is node +,-, ... (based on defines iks_ast.h)
   {
		son1=node->son_list[0]->nodeSymbol;
   }
   else if(node->son_list[0]->nodeType==27)
   {
               son1 = node -> son_list[0]->son_list[0]->dictPointer->itemType;
               //printf("\n %d",son1);
   }
   else
   {
		son1=node->son_list[0]->dictPointer->itemType; // get symbol (float, int, etc) from son1
   }
   
   if((node->son_list[1]->nodeType >= 12)&&( node->son_list[1]->nodeType <= 15))// verify if son2 is node +,-, ...
   {	
		son2=node->son_list[1]->nodeSymbol;
   }
   else if(node->son_list[1]->nodeType==27)
   {
   
                 son2 = node -> son_list[1]->son_list[0]->dictPointer->itemType;
                 //printf("\n %d",son2);
   }
   else
   {
		son2=node->son_list[1]->dictPointer->itemType; //get symbol (float, int, etc) from son2
   }

  
///////////////////////////////// Inference Rules /////////////////////////////////////////////////

   if(	(son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==278 && son2==278) )// if both sons are float.
   {
     node->nodeSymbol=IKS_SIMBOLO_FLOAT;//father is float 
     node->nodeSize= 8;//float uses 8 bytes    
    }

    if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_INT)
		|| (son1==277 && son2==278)
		|| (son1==278 && son2==277)
		|| (son1==277 && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==278 && son2==IKS_SIMBOLO_INT)
		|| (son1==IKS_SIMBOLO_INT && son2==278)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==277))// if one son is float...
   {
     node->nodeSymbol=IKS_SIMBOLO_FLOAT;//father is float
     node->nodeSize= 8;//float uses 8 bytes
   }

   if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_INT)
		|| (son1==277 && son2==277))// if both sons are int...
   {
     
    node->nodeSymbol=IKS_SIMBOLO_INT;//father is int 
    node->nodeSize= 4;//int uses 4 bytes
   }

  if(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_BOOL)// if both sons are bool...
   {   
    node->nodeSymbol=IKS_SIMBOLO_BOOL;//father is bool
    node->nodeSize= 1;//int uses 1 byte
   }

  if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_INT)
		|| (son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_BOOL)
		|| (son1==IKS_SIMBOLO_BOOL && son2==277)
		|| (son1==277 && son2==IKS_SIMBOLO_BOOL))// bool and int...
   {
    node->nodeSymbol=IKS_SIMBOLO_INT;//father is int
    node->nodeSize= 4;//int uses 4 bytes
   }

 if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_BOOL)
		|| (son1==IKS_SIMBOLO_BOOL && son2==278)
		|| (son1==278 && son2==IKS_SIMBOLO_BOOL))// bool and float...
   {
    node->nodeSymbol=IKS_SIMBOLO_FLOAT;//father is int
    node->nodeSize= 8;//float uses 8 bytes
   }
//////////////////////////////////////////////////////// coercion rules //////////////////////////////////////////////////
  
     if((son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_FLOAT)   // se o filho da direita for float e o filho da esquerda for int
        ||(son1==277 && son2==278)                           // entao, o filho da esquerda (int) deve virar float na etapa de geração de codigo
        ||(son1==277 && son2==IKS_SIMBOLO_FLOAT)
        ||(son1==IKS_SIMBOLO_INT && son2==278))
      {   
	node->son_list[0]->dictPointer->cast = CAST_INT_TO_FLOAT;  
      }
      
      if((son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_INT)   // se o filho da direita for int e o filho da esquerda for bool
        ||(son1==IKS_SIMBOLO_BOOL && son2==277))             // entao, o filho da esquerda (bool) deve virar int na etapa de geração de codigo  
      {   
	node->son_list[0]->dictPointer->cast = CAST_BOOL_TO_INT;  
      }

     if((son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_FLOAT)   // se o filho da direita for float e o filho da esquerda for bool
        ||(son1==IKS_SIMBOLO_BOOL && son2==278))             // entao, o filho da esquerda (bool) deve virar float na etapa de geração de codigo  
      {   
	node->son_list[0]->dictPointer->cast = CAST_BOOL_TO_FLOAT;  
      }

     

                        
//////////////////////////////////////////////////////// errors: incompatible symbols /////////////////////////////////////                                
  
   if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_CHAR)
		||(son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_INT)
		|| (son1==277 && son2==281)
		|| (son1==281 && son2==277)
		|| (son1==IKS_SIMBOLO_INT && son2==281)
		|| (son1==IKS_SIMBOLO_CHAR && son2==277)
		|| (son1==277 && son2==IKS_SIMBOLO_CHAR)
		|| (son1==281 && son2==IKS_SIMBOLO_INT)) // char and int => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: char e int\n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

  if(	(son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_CHAR)
		||(son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==278 && son2==281)
		|| (son1==281 && son2==278)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==281)
		|| (son1==IKS_SIMBOLO_CHAR && son2==278)
		|| (son1==278 && son2==IKS_SIMBOLO_CHAR)
		|| (son1==281 && son2==IKS_SIMBOLO_FLOAT)) // char and float => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: char e float \n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

  if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_CHAR)
		|| (son1==IKS_SIMBOLO_CHAR && son2==IKS_SIMBOLO_BOOL)
		|| (son1==IKS_SIMBOLO_BOOL && son2==281)
		|| (son1==281 && son2==IKS_SIMBOLO_BOOL)) // char and bool => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: char e bool \n");
                #endif
                exit(IKS_ERROR_CHAR_TO_X);
   }

  if(	(son1==IKS_SIMBOLO_INT && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_INT)
		|| (son1==277 && son2==282)
		|| (son1==282 && son2==277)
		|| (son1==277 && son2==IKS_SIMBOLO_STRING)
		|| (son1==282 && son2==IKS_SIMBOLO_INT)
		|| (son1==IKS_SIMBOLO_STRING && son2==277)
		|| (son1==IKS_SIMBOLO_INT && son2==282)) // String and int => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: string e int \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X );
   }
   
   if(	(son1==IKS_SIMBOLO_FLOAT && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==278 && son2==282)
		|| (son1==282 && son2==278)
		|| (son1==278 && son2==IKS_SIMBOLO_STRING)
		|| (son1==282 && son2==IKS_SIMBOLO_FLOAT)
		|| (son1==IKS_SIMBOLO_STRING && son2==278)
		|| (son1==IKS_SIMBOLO_FLOAT && son2==282)) // String and float => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: string e float \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X );
   }

   if(	(son1==IKS_SIMBOLO_BOOL && son2==IKS_SIMBOLO_STRING)
		|| (son1==IKS_SIMBOLO_STRING && son2==IKS_SIMBOLO_BOOL)
		|| (son1==282 && IKS_SIMBOLO_BOOL)
		|| (son1==IKS_SIMBOLO_BOOL && son2==282)) // String and bool => incompatible
   {
                #ifdef SYNTAX_DEBUG
                        printf("ERRO: Tipos incompatíveis: string e bool \n");
                #endif
                exit(IKS_ERROR_STRING_TO_X );
   }

}


void verifyReturn(COMP_DICT_ITEM_T *function, COMP_TREE_T *expression)
{	
	if(expression->nodeType == 10) //If the node's type is IKS_AST_IDENTIFICADOR
	{
		if((function->itemType == IKS_SIMBOLO_INT) && (expression->dictPointer->itemType != 277) //TK_LIT_INT 277
												   && (function->itemType != expression->dictPointer->itemType)
												   && (expression->dictPointer->itemType != 278) //TK_LIT_FLOAT 278
												   && (expression->dictPointer->itemType != IKS_SIMBOLO_FLOAT) //CAST_FLOAT_TO_INT
												   && (expression->dictPointer->itemType != 279) //TK_LIT_FALSE 279
												   && (expression->dictPointer->itemType != 280) //TK_LIT_TRUE 280
												   && (expression->dictPointer->itemType != IKS_SIMBOLO_BOOL)) //CAST_BOOL_TO_INT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_FLOAT) && (expression->dictPointer->itemType != 278) //TK_LIT_FLOAT 278
													 && (function->itemType != expression->dictPointer->itemType)
													 && (expression->dictPointer->itemType != 277) //TK_LIT_INT 277
													 && (expression->dictPointer->itemType != IKS_SIMBOLO_INT) //CAST_INT_TO_FLOAT
													 && (expression->dictPointer->itemType != 279) //TK_LIT_FALSE 279
													 && (expression->dictPointer->itemType != 280) //TK_LIT_TRUE 280
													 && (expression->dictPointer->itemType != IKS_SIMBOLO_BOOL)) //CAST_BOOL_TO_FLOAT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_CHAR) && (expression->dictPointer->itemType != 281)
													&& (function->itemType != expression->dictPointer->itemType)) //TK_LIT_CHAR 281
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_STRING) && (expression->dictPointer->itemType != 282)
													  && (function->itemType != expression->dictPointer->itemType)) //TK_LIT_STRING 282
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_BOOL) && ((expression->dictPointer->itemType != 279) //TK_LIT_FALSE 279
														|| (expression->dictPointer->itemType != 280)) //TK_LIT_TRUE 280
													&& (function->itemType != expression->dictPointer->itemType)
													&& (expression->dictPointer->itemType != 277) //TK_LIT_INT 277
													&& (expression->dictPointer->itemType != 278) //TK_LIT_FLOAT 278
													&& (expression->dictPointer->itemType != IKS_SIMBOLO_INT) //CAST_INT_TO_BOOL
													&& (expression->dictPointer->itemType != IKS_SIMBOLO_FLOAT)) //CAST_FLOAT_TO_BOOL
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
	}
	else if(expression->nodeType == 27) //If the node's type is IKS_AST_CHAMADA_DE_FUNCAO
	{
		if((function->itemType == IKS_SIMBOLO_INT) && (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_INT)
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_FLOAT) //CAST_FLOAT_TO_INT
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_BOOL)) //CAST_BOOL_TO_INT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		
		if((function->itemType == IKS_SIMBOLO_FLOAT) && (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_FLOAT)
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_INT) //CAST_INT_TO_FLOAT
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_BOOL)) //_CAST_BOOL_TO_FLOAT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_BOOL) && (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_BOOL)
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_INT) //CAST_INT_TO_BOOL
													&& (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_FLOAT)) //CAST_FLOAT_TO_BOOL
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		
		if((function->itemType == IKS_SIMBOLO_CHAR) && (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_CHAR)) //There is not cast for char
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_STRING) && (expression->son_list[0]->dictPointer->itemType != IKS_SIMBOLO_STRING)) //There is not cast for string
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
	}
	else //Expression node
	{	
		if((function->itemType == IKS_SIMBOLO_INT) && (expression->nodeSymbol != IKS_SIMBOLO_INT)
													&& (expression->nodeSymbol != IKS_SIMBOLO_FLOAT) //CAST_FLOAT_TO_INT
													&& (expression->nodeSymbol != IKS_SIMBOLO_BOOL)) //CAST_BOOL_TO_INT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}	
		if((function->itemType == IKS_SIMBOLO_FLOAT) && (expression->nodeSymbol != IKS_SIMBOLO_FLOAT)
													&& (expression->nodeSymbol != IKS_SIMBOLO_INT) //CAST_INT_TO_FLOAT
													&& (expression->nodeSymbol != IKS_SIMBOLO_BOOL)) //CAST_BOOL_TO_FLOAT
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_BOOL) && (expression->nodeSymbol != IKS_SIMBOLO_BOOL)
													&& (expression->nodeSymbol != IKS_SIMBOLO_INT) //CAST_INT_TO_BOOL
													&& (expression->nodeSymbol != IKS_SIMBOLO_FLOAT)) //CAST_FLOAT_TO_BOOL
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_CHAR) && (expression->nodeSymbol != IKS_SIMBOLO_CHAR)) //There is not cast for char
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
		if((function->itemType == IKS_SIMBOLO_STRING) && (expression->nodeSymbol != IKS_SIMBOLO_STRING)) //There is not cast for string
		{
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
	}
}

void verifyParameters(COMP_DICT_T *dict, char *key, COMP_LIST_T *listOfParameters)
{
	COMP_DICT_ITEM_T* function;
	COMP_LIST_T *functionParameters;
	COMP_LIST_T *passedParameters;

	function = get_dictionary_item(dict->nextDict, key);
	
	functionParameters = function->parameterList;
	passedParameters = listOfParameters;
	
	if((function != NULL) && (function->isFunction))
	{
		while((function->parameterList != NULL) && (listOfParameters != NULL))
		{
			function->parameterList = function->parameterList->next;
			listOfParameters = listOfParameters->next;
		}
		
		if((function->parameterList == NULL) && (listOfParameters != NULL))
		{
			exit(IKS_ERROR_EXCESS_ARGS);
		}
		else if((listOfParameters == NULL) && (function->parameterList != NULL))
		{
			exit(IKS_ERROR_MISSING_ARGS);
		}
		else
		{	
			while((functionParameters != NULL) && (passedParameters != NULL))
			{
				if((((COMP_DICT_ITEM_T *)functionParameters->data)->itemType == IKS_SIMBOLO_INT)
					&& (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 277)
					&& (((COMP_DICT_ITEM_T *)functionParameters->data)->itemType !=
					   (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType)))
				{
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				}
				if((((COMP_DICT_ITEM_T *)functionParameters->data)->itemType == IKS_SIMBOLO_FLOAT)
					&& (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 278)
					&& (((COMP_DICT_ITEM_T *)functionParameters->data)->itemType !=
					   (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType)))
				{
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				}
				if((((COMP_DICT_ITEM_T *)functionParameters->data)->itemType == IKS_SIMBOLO_CHAR)
					&& (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 281)
					&& (((COMP_DICT_ITEM_T *)functionParameters->data)->itemType !=
					   (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType)))
				{
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				}
				if((((COMP_DICT_ITEM_T *)functionParameters->data)->itemType == IKS_SIMBOLO_STRING)
					&& (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 282)
					&& (((COMP_DICT_ITEM_T *)functionParameters->data)->itemType !=
					   (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType)))
				{
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				}
				if((((COMP_DICT_ITEM_T *)functionParameters->data)->itemType == IKS_SIMBOLO_BOOL)
					&& ((((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 279)
						|| (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType != 280))
					&& (((COMP_DICT_ITEM_T *)functionParameters->data)->itemType !=
					   (((COMP_TREE_T *)passedParameters->data)->dictPointer->itemType)))
				{
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				}
				
				functionParameters = functionParameters->next;
				passedParameters = passedParameters->next;
			}
			
			return;
		}
	}
	
	return;
}

void verifyDeclaration(COMP_DICT_T* dict, char* key){
	
	COMP_DICT_ITEM_T* search;

	search = get_dictionary_item(dict, key);

	/* Double declaration */
	if(search != NULL){ // Already in dict (and just the first dict, since we can have a global and local ID.)

		#ifdef SYNTAX_DEBUG
			printf("ERRO: Dupla declaração de %s encontrada. \n", search->key);
		#endif
		exit(IKS_ERROR_DECLARED);
	}
}

void verifyFunction(COMP_DICT_T *dict, char *key)
{
	COMP_DICT_ITEM_T* search;

		search = get_dictionary_item(dict, key);

		/* Not found */
		if(search == NULL)
		{
			if(dict->nextDict == NULL)
			{
				exit(IKS_ERROR_UNDECLARED);
			}
			else
			{
				search = get_dictionary_item(dict->nextDict, key);
				if(search == NULL)
				{
					exit(IKS_ERROR_UNDECLARED);
				}
			}
		}

		/* Is a variable */
		if(search->isVector == 0 && search->isFunction == 0)
		{
			exit(IKS_ERROR_VARIABLE);
		}

		/* Is a vector */
		if(search->isVector == 1 && search->isFunction == 0)
		{
			exit(IKS_ERROR_VECTOR);
		}

		/* Finally, it is a function- PASS*/
}
	
void verifyVariable(COMP_DICT_T *dict, char *key)
{
		COMP_DICT_ITEM_T* search;

		search = get_dictionary_item(dict, key);

		/* Not found */
		if(search == NULL)
		{
			if(dict->nextDict == NULL)
			{
				exit(IKS_ERROR_UNDECLARED);
			}
			else
			{
				search = get_dictionary_item(dict->nextDict, key);
				if(search == NULL)
				{
					exit(IKS_ERROR_UNDECLARED);
				}
			}
		}

		/* Is a function */
		if(search->isVector == 0 && search->isFunction == 1)
		{
			exit(IKS_ERROR_FUNCTION);
		}

		/* Is a vector */
		if(search->isVector == 1 && search->isFunction == 0)
		{
			exit(IKS_ERROR_VECTOR);
		}

		/* Finally, it is a variable- PASS*/
}

void verifyVector(COMP_DICT_T* dict, char* key){

		COMP_DICT_ITEM_T* search;

		search = get_dictionary_item(dict, key);

		/* Not found */
		if(search == NULL)
		{
			if(dict->nextDict == NULL)
			{
				exit(IKS_ERROR_UNDECLARED);
			}
			else
			{
				search = get_dictionary_item(dict->nextDict, key);
				if(search == NULL)
				{
					exit(IKS_ERROR_UNDECLARED);
				}
			}
		}

		/* Is a variable */
		if(search->isVector == 0 && search->isFunction == 0)
		{
			exit(IKS_ERROR_VARIABLE);
		}

		/* Is a function */
		if(search->isVector == 0 && search->isFunction == 1)
		{
			exit(IKS_ERROR_FUNCTION);
		}

		/* Finally, it is a vector - PASS*/
}

void verifyFunctionCall(COMP_DICT_T* dict, char* key, COMP_TREE_T* parametersTree){} //Not implemented

void verifyID(COMP_DICT_T* dict, char* key){

 		COMP_DICT_ITEM_T* search;

 		/* Search in current dictionary */
		search = get_dictionary_item(dict, key);

		if(search != NULL){
			return; /* Found */
		}

		/* If is in local context, search in global dict */
		if(dict->nextDict != NULL){
			search = get_dictionary_item(dict->nextDict, key);

			if(search != NULL){
				return; /* Found */
			}

		}

		/* Not found */
		#ifdef SYNTAX_DEBUG
			printf("ERRO: %s não foi declarada. \n", key);
		#endif
		exit(IKS_ERROR_UNDECLARED);
 }
