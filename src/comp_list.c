#include <stdlib.h>
#include "comp_list.h"

/* ========== Internal functions declaration ========== */

/**
	Free memory for data and list node.	
*/
void delete_node(COMP_LIST_T* target_node);
/**
	Create a list node with the data.
*/
COMP_LIST_T* create_list_node(void* data_target);

/* ========== Public functions implementation ========== */

COMP_LIST_T* create_list(){
	/* Actually doesn't need to create nothing */
	return NULL;
}

COMP_LIST_T* remove_list(COMP_LIST_T* target_list){
	COMP_LIST_T* target = target_list;
	COMP_LIST_T* next;

	/* Empty list */
	if(target == NULL){
		return NULL;	
	}

	/* Remove loop */
	do{	
		next = target->next;
		delete_node(target);
		target = next;
	}while(target != NULL);

	return NULL;
}

COMP_LIST_T* find_node(COMP_LIST_T* target_list, int index){
	int counter = 0;
	COMP_LIST_T* target = target_list;

	/* Index value validation*/
	if(index < 0){
		return NULL;	
	}

	/* Find the node */
	while(target != NULL && counter < index){
		target = target->next;
		counter++;
	}
	
	return target;
}

COMP_LIST_T* insert(COMP_LIST_T* target_list, int index, void* data){
	COMP_LIST_T* target = target_list;
	COMP_LIST_T* before = NULL;
	COMP_LIST_T* new_node;	

	int counter = 0;

	/* Index value validation */
	if(index < 0){
		return target_list; /* Don't add in list*/	
	}

	/* Create the list node with the data */
	new_node = create_list_node(data);

	/* Empty list */
	if(target == NULL){
		return new_node; /* The list now is the node */	
	}

	/* Find the place to add the new node */
	while(target != NULL && counter < index){
		before = target;		
		target = target->next;
		counter++;
	}

	/* Add in list */
	if(before != NULL){
		before->next = new_node; 
	}
	else{
		target_list = new_node;	/* It's the first one */
	}
	new_node->next = target;
	
	return target_list;
}

COMP_LIST_T* insert_last(COMP_LIST_T* target_list, void* data){
	COMP_LIST_T* new_node;	
	COMP_LIST_T* target = target_list;

	/* Create list node */	
	new_node = create_list_node(data);	
	
	/* Empty list */
	if(target_list == NULL){
		return new_node; /* The list now is the node */	
	}

	/* Find the last element in list */
	while(target->next != NULL){
		target = target->next;
	}

	/* Link in list */
	target->next = new_node;

	return target_list;
}

COMP_LIST_T* remove_node(COMP_LIST_T* target_list, COMP_LIST_T* target_node){
	COMP_LIST_T* before;
	COMP_LIST_T* target;

	/* No elements in list */
	if(target_list == NULL){
		return NULL;
	}

	/* Only one element in list*/
	if(target_list->next == NULL){
		/* Check if is the node */
		if(target_node->data == target_list->data){
			delete_node(target_list);			
			return NULL;
		}
	}

	/* There's more than 1 element */
	/* The first one is the target */
	if(target_node->data == target_list->data){
			target = target_list->next;
			delete_node(target_list);
			target_list = target;			
			return target_list;
	}
	
	/* The target is other node */
	target = target_list;
	do{
		before = target;	
		target = target->next;
	}while(target->data != target_node->data && target_list->next != NULL);

	/* If the node was not found */
	if(target->data != target_node->data){
		return target_list; /* Not found */
	}

	/* Remove the node */
	before->next = target->next;
	delete_node(target);
	return target_list;
}

/* ========== Internal functions implementations ========== */

void delete_node(COMP_LIST_T* target_node){
	free(target_node->data);
	free(target_node);
	
	return;	
}

COMP_LIST_T* create_list_node(void* data_target){
	COMP_LIST_T* new_node;	

	new_node = malloc(sizeof(COMP_LIST_T));
	new_node->data = data_target;
	new_node->next = NULL;

	return new_node;
}
