/*
   main.c

   Arquivo principal do analisador sintático.
*/
#include "main.h"
//#define SHOW_ERROR


void yyerror (char const *mensagem)
{
  #ifdef SHOW_ERROR
  fprintf (stderr, "%s line: %i \n", mensagem, getLineNumber());
  #endif
}

int main (int argc, char **argv)
{
	extern COMP_DICT_T *dict;
  extern COMP_TREE_T* root;
	  init_dict();

  	gv_init("resultGraph.dot");


  	int resultado = yyparse();
  	
    draw_tree(root);
  	//printDict(dict);

    gv_close();


  	return resultado;
}

