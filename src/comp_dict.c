#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "comp_dict.h"
#include "main.h"
#include "parser.h"


#ifndef BOOL_VALUE
#define BOOL_TRUE 1
#define BOOL_FALSE 0
#endif

/* ========== Internal functions declaration ========== */

/**
	Calculates hash.
 */
int hash(char* key);


/**
	Search for a node
*/
COMP_DICT_ITEM_T* search_list_node(COMP_DICT_T* target_dict, char* key);

/* ========== External functions implementation ========== */

COMP_DICT_T* create_dictionary(int hashtable_size, COMP_DICT_T* nextDict){
	
	COMP_DICT_T* new_dic;
	int i;
	
	/* Create dictionary */
	if( (new_dic = malloc(sizeof(COMP_DICT_T))) == NULL ) return NULL;
		
	/* Create hashtable */
	if( (new_dic->table = malloc( ( sizeof(COMP_DICT_ITEM_T*) )*hashtable_size ) ) == NULL ) return NULL;
	
	/* Clear all table rows */
	for(i = 0; i < hashtable_size; i++){
		new_dic->table[i] = NULL;
	}

	/* Define hashtable size */
	new_dic->size = hashtable_size; 
	new_dic->nextDict = nextDict;
	
	return new_dic;
}

COMP_DICT_ITEM_T* insert_dictionary_item(COMP_DICT_T* dictionary, char* key, int itemType, int lineNumber){
	
	int address;
	COMP_DICT_ITEM_T* list_pointer;
	COMP_DICT_ITEM_T* new_node;

	/*If already in list, return*/
	list_pointer = search_list_node(dictionary, key);
	if(list_pointer != NULL){
		return list_pointer;
	}
	
	/* Find the right address */
	address = hash(key) % dictionary->size;

	/* Get the list pointer */
	list_pointer = dictionary->table[address];
	
	/* Empty list - create the first node */
	if(list_pointer == NULL){
		list_pointer = malloc(sizeof(COMP_DICT_ITEM_T));
		list_pointer ->key = strdup(key);
		list_pointer->itemType = itemType;
		list_pointer->lineNumber = lineNumber;
		list_pointer->next = NULL;

		dictionary->table[address] = list_pointer;
		
		return list_pointer;
	}
	
	/* Search the last in list */
	while(list_pointer->next != NULL){
		if( strcmp(list_pointer->key, key) == 0) {
			return list_pointer; 	/* Key already in list */
		}

		list_pointer = list_pointer->next;
	}
	
	/* Create a new node */
	new_node = malloc(sizeof(COMP_DICT_ITEM_T));
	new_node->key = strdup(key);
	new_node->itemType = itemType;
	new_node->lineNumber = lineNumber;
	new_node->next = NULL;

	
	/* Add to list */
	list_pointer->next = new_node;

	return new_node;
	
}

int search_dictionary_item(COMP_DICT_T* target_dict, char* key){
	
	int address;
	COMP_DICT_ITEM_T* list_pointer;
	
	/* Search item */
	list_pointer = search_list_node(target_dict, key);
	if( list_pointer == NULL ){
		return ERROR_CODE;
	}

	/* Key found */
      return list_pointer->lineNumber; 	/* Key found */
	
}

COMP_DICT_ITEM_T* get_dictionary_item(COMP_DICT_T* target_dict, char* key){
	int address;
	COMP_DICT_ITEM_T* list_pointer;
	
	/* Search item */
	list_pointer = search_list_node(target_dict, key);
	if( list_pointer == NULL ){
		return NULL;
	}

	/* Key found */
      return list_pointer; 	/* Key found */
}

int remove_dictionary_item(COMP_DICT_T* target_dict, char* key){
  
 
        int address;
        int lineNumber;
        COMP_DICT_ITEM_T* list_pointer;
        COMP_DICT_ITEM_T* before_pointer;
        
        /* Find the right address */
        address = hash(key) % target_dict->size;
        
        /* Get the list pointer */
        list_pointer = target_dict->table[address];
        before_pointer = target_dict->table[address];
        
        /* Empty list - no results */
        if(list_pointer == NULL){
                return ERROR_CODE;        
        }
        
        /* Search in list */
        while(list_pointer != NULL){
          
                if( strcmp(list_pointer->key, key) == 0){ /* Key found */
                        lineNumber = list_pointer->lineNumber;
                        
                        /* Case 1 - First of list */
                        if(before_pointer == list_pointer){
                                target_dict->table[address] = list_pointer->next; /* Remove from list */
                        }
                        else{/* Case 2 - Rest of the list */
                                before_pointer->next = list_pointer->next; /* Remove from list */
                        }
                        
                        free(list_pointer); /* Remove node */
                        
                        return lineNumber;  
                } 
                
                before_pointer = list_pointer;
                list_pointer = list_pointer->next;
        }
  
        return ERROR_CODE;
	
}

COMP_DICT_ITEM_T* change_dictionary_item_itemValue(COMP_DICT_T* target_dict, char* key, int itemType){
  	int address;
	int lineNumber;
	COMP_DICT_ITEM_T* list_pointer;
	
	/* Search item */
	list_pointer = search_list_node(target_dict, key);
	if( list_pointer == NULL ){
		return list_pointer;
	}

	/* Key found */
	list_pointer->itemType = itemType;
	return list_pointer;

}

COMP_DICT_ITEM_T* change_dictionary_item_int(COMP_DICT_T* target_dict, char* key, char* intValue){
	
	COMP_DICT_ITEM_T* list_pointer;
	int newValue;

	/* Find node */
	list_pointer = search_list_node(target_dict, key);
	if(list_pointer == NULL){
		return list_pointer;
	}

	/* Get the value */
	sscanf(intValue, "%i", &newValue);

	list_pointer->intValue = newValue;

	return list_pointer;
}

COMP_DICT_ITEM_T* change_dictionary_item_float(COMP_DICT_T* target_dict, char* key, char* floatValue){
	COMP_DICT_ITEM_T* list_pointer;
	float newValue;

	/* Find node */
	list_pointer = search_list_node(target_dict, key);
	if(list_pointer == NULL){
		return list_pointer;
	}

	/* Get the value */
	sscanf(floatValue, "%f", &newValue);

	list_pointer->floatValue = newValue;

	return list_pointer;
}

COMP_DICT_ITEM_T* change_dictionary_item_char(COMP_DICT_T* target_dict, char* key, char* charValue){
	COMP_DICT_ITEM_T* list_pointer;
	char newValue;

	/* Find node */
	list_pointer = search_list_node(target_dict, key);
	if(list_pointer == NULL){
		return list_pointer;
	}

	/* Get the value */
	sscanf(charValue, "\' %c \' ", &newValue);

	list_pointer->charValue = newValue;

	return list_pointer;
}

COMP_DICT_ITEM_T* change_dictionary_item_string(COMP_DICT_T* target_dict, char* key, char* stringValue){
	COMP_DICT_ITEM_T* list_pointer;
	char* newValue;

	/* Find node */
	list_pointer = search_list_node(target_dict, key);
	if(list_pointer == NULL){
		return list_pointer;
	}

	newValue = strdup(stringValue);

	/* Remove double quotes */
	newValue = strtok(newValue,"\"");
	
	/* Get the value */
	list_pointer->stringValue = strdup(newValue);

	return list_pointer;
}

COMP_DICT_ITEM_T* change_dictionary_item_bool(COMP_DICT_T* target_dict, char* key, char* boolValue){

	COMP_DICT_ITEM_T* list_pointer;


	/* Find node */
	list_pointer = search_list_node(target_dict, key);
	if(list_pointer == NULL){
		return list_pointer;
	}

	if( strcmp(boolValue, "true") == 0 ){
		
		list_pointer->boolValue = BOOL_TRUE;
		return list_pointer;

	}else if ( strcmp(boolValue, "false") == 0 ){
		
		list_pointer->boolValue = BOOL_FALSE;
		return list_pointer;

	}

	/*Cannot pass here*/
	fprintf(stderr, "NO BOOL VALUE IN BOOL PLACE");

	return NULL;
}

void printDict(COMP_DICT_T* target_dict){

	int address;
	int i;

	COMP_DICT_ITEM_T* list_pointer;
	COMP_DICT_ITEM_T* function_item;
	COMP_LIST_T* function_list_pointer;
	
	for(i=0; i< target_dict->size; i++){	
		/* Get the list pointer */
		list_pointer = target_dict->table[i];

		/* Search in list */
		while(list_pointer != NULL){
		  
		  	/* Print hash elements */
		  	printf("\n=================\n");

		  	if(list_pointer->isFunction){
		  		printf("FUNCAO \n");
		  	}else if(list_pointer->isVector){
		  		printf("VETOR \n");
		  	} else{
		  		printf("VARIAVEL \n");
		  	}

			printf(" Name: %s \n ItemType:  %i \n LineNumer: %i \n", list_pointer->key, list_pointer->itemType, list_pointer->lineNumber);
			switch( list_pointer->itemType ){
				case TK_LIT_INT: 			printf("INT: %i \n", list_pointer->intValue); break;
				case TK_LIT_FLOAT: 		printf("FLOAT: %f \n", list_pointer->floatValue); break;
				case TK_LIT_CHAR: 		printf("CHAR: %c \n", list_pointer->charValue); break;
				case TK_LIT_STRING: 		printf("STRING: %s \n", list_pointer->stringValue); break;
				case TK_LIT_TRUE: 		printf("TRUE \n"); break;
				case TK_LIT_FALSE: 		printf("FALSE \n"); break;

				case IKS_SIMBOLO_INT:
				case IKS_SIMBOLO_FLOAT:
				case IKS_SIMBOLO_CHAR:
				case IKS_SIMBOLO_STRING:
				case IKS_SIMBOLO_BOOL:		printf("IDENTIFIER FROM TYPE: %i \n", list_pointer->itemType);break;

				default: printf("DONT KNOW \n");
			}

			if(list_pointer->isFunction){
				if(list_pointer->ready){

					function_list_pointer = list_pointer->parameterList;

					printf("\n LISTA: ");

					while(function_list_pointer != NULL){
						function_item = function_list_pointer->data;
						printf("%s : %i; ", function_item->key, function_item->itemType);
						function_list_pointer = function_list_pointer->next;
					}

				}else{
					printf("NÃO PRONTA\n");
				}
		  	}

			printf("\n=================\n");
		  	/* End print hash */
			
			list_pointer = list_pointer->next;
		}
	}

	if(target_dict->nextDict != NULL){
		printf( "\n ****************** NEXT DICT ****************** \n");
		printDict(target_dict->nextDict);
	}

}

/* ========== Internal functions declaration ========== */

int hash(char* key){
    int hash = 2013;
    int c;

    while (c = *key++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return abs(hash); /* Hash can't be negative */
}


COMP_DICT_ITEM_T* search_list_node(COMP_DICT_T* target_dict, char* key){
  	int address;
	COMP_DICT_ITEM_T* list_pointer;
	COMP_DICT_ITEM_T* before_pointer;

	/* Find the right address */
	address = hash(key) % target_dict->size;
	
	/* Get the list pointer */
	list_pointer = target_dict->table[address];
	before_pointer = target_dict->table[address];
	
	/* Empty list - no results */
	if(list_pointer == NULL){
		return NULL;	  
	}
	
	/* Search in list */
	while(list_pointer != NULL){
	  
		if( strcmp(list_pointer->key, key) == 0){ /* Key found */
			return list_pointer;
		}
		
		before_pointer = list_pointer;
		list_pointer = list_pointer->next;
	}
  	return NULL;
}