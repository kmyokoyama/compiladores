#ifndef COMP_TREE
#define COMP_TREE

#include <stdarg.h> /*<! For variable arguments in add function */
#include "comp_dict.h"

#define MAX_TREE_SONS 5

#define IKS_SIMBOLO_INT    1
#define IKS_SIMBOLO_FLOAT  2
#define IKS_SIMBOLO_CHAR   3
#define IKS_SIMBOLO_STRING 4
#define IKS_SIMBOLO_BOOL   5

/**
 *	Type for trees
 */
typedef struct COMP_TREE_S COMP_TREE_T;

struct COMP_TREE_S
{
	int nodeType;   //nodeType refers to defines of iks_ast.h
	int nodeSize;
    int nodeSymbol; // nodeSymbol refers to float, int, char, etc
	char* name;

	COMP_DICT_ITEM_T* dictPointer;
	COMP_TREE_T* son_list[MAX_TREE_SONS]; /*!< List of sons pointers */
};

/**
 *	Create a ast node and link with its sons. Uses stdarg.h for variable parameters.
 */
COMP_TREE_T* create_ast_node(int nodeType, COMP_DICT_ITEM_T* dictPointer, char* name, int numSons, ... );

/**
 *	Add sons
 */
COMP_TREE_T* add_son_ast_node(COMP_TREE_T* target_node, int numSons, ... );

/**
 *	Use gv to draw the tree
 */
void draw_tree(COMP_TREE_T* root_node);

#endif