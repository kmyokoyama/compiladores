#ifndef COMP_LIST
#define COMP_LIST


/**
	Type for list nodes.
 */
typedef struct LIST_STRUCT COMP_LIST_T;

/**
	 Struct used for list nodes.
 */
struct LIST_STRUCT{
	void* data;
	COMP_LIST_T* next;
};

/**
	Create a empty list.
	@return Empty list pointer
 */
COMP_LIST_T* create_list();

/**
	Destroy target_list, removing all its nodes.
 */
COMP_LIST_T* remove_list(COMP_LIST_T* target_list);

/**
	Find the node in the index position (starts at 0).
	@return The node in list with the index position. Can be used to get a sublist, since the list is still linked.
 */
COMP_LIST_T* find_node(COMP_LIST_T* target_list, int index);

/**
	Insert data into the given index position. If index is negative, don't add to list.
	@return List pointer
 */
COMP_LIST_T* insert(COMP_LIST_T* target_list, int index, void* data);

/**
	Insert the data in the end of the list
	@return List pointer
 */
COMP_LIST_T* insert_last(COMP_LIST_T* target_list, void* data);

/**
	Remove the node in the index position from the list.
	@return List pointer (not the data pointer - use find_node() before to get it).
 */
COMP_LIST_T* remove_node(COMP_LIST_T* target_list, COMP_LIST_T* target_node);

#endif
