#include "comp_list.h"

#ifndef COMP_DICT
#define COMP_DICT

#define ERROR_CODE 0

#ifndef IKS_SIMBOLO
#define IKS_SIMBOLO

#endif

#define CAST_INT_T0_FLOAT 1
#define CAST_INT_TO_BOOL 2
#define CAST_FLOAT_TO_INT 3
#define CAST_FLOAT_TO_BOOL 4
#define CAST_BOOL_TO_INT 5
#define CAST_BOOL_TO_FLOAT 6

/**
 * Data node type for dictionary.
 */
typedef struct COMP_DICT_ITEM_S COMP_DICT_ITEM_T;

struct COMP_DICT_ITEM_S{
	char* key;
	int itemType;
	int lineNumber;
    int size; // refere-se à quantidade de bytes necessários para armazenar o dado
    int cast; // cast (marca referente a coerção que deverá ocorrer)

	union{
		int intValue;
		float floatValue;
		char charValue;
		char* stringValue;
		char boolValue;
	};

	int isVector;
	int isFunction;

	int ready;

	COMP_LIST_T* parameterList; /* For functions */

	COMP_DICT_ITEM_T* next;
};

/**
 * Data table type for dictionary.
 */
typedef struct COMP_DICT_S COMP_DICT_T;

struct COMP_DICT_S{
      int size;				/*!< Table size */
      COMP_DICT_ITEM_T** table; 	/*!< A vector of pointers to COMP_DICT_ITEM_T type */

	COMP_DICT_T* nextDict;	/*<! Pointer to the next dictionary */
};


/**
	Creates a new dictionary.
	@param Size for hashtable
*/
COMP_DICT_T* create_dictionary(int hashtable_size, COMP_DICT_T* nextDict);

/**
	Insert a new item in a dictionary.
*/
COMP_DICT_ITEM_T* insert_dictionary_item(COMP_DICT_T* dictionary, char* key, int itemType, int lineNumber);

/**
	Search a dictionary item by a key and returns its value.
*/
int search_dictionary_item(COMP_DICT_T* target_dict, char* key);

/**
	Get a dictionary item.
*/
COMP_DICT_ITEM_T* get_dictionary_item(COMP_DICT_T* target_dict, char* key);

/**
	Remove an item from a dictionary.
*/
int remove_dictionary_item(COMP_DICT_T* target_dict, char* key);

/**
 	Change itemType value
 */
COMP_DICT_ITEM_T* change_dictionary_item_itemValue(COMP_DICT_T* target_dict, char* key, int itemType);

/**
 	Prints hash
 */
void printDict(COMP_DICT_T* target_dict);

/**
	Value modifiers
  */
COMP_DICT_ITEM_T* change_dictionary_item_int(COMP_DICT_T* target_dict, char* key, char* intValue);
COMP_DICT_ITEM_T* change_dictionary_item_float(COMP_DICT_T* target_dict, char* key, char* floatValue);
COMP_DICT_ITEM_T* change_dictionary_item_char(COMP_DICT_T* target_dict, char* key, char* charValue);
COMP_DICT_ITEM_T* change_dictionary_item_string(COMP_DICT_T* target_dict, char* key, char* stringValue);
COMP_DICT_ITEM_T* change_dictionary_item_bool(COMP_DICT_T* target_dict, char* key, char* boolValue);

#endif
