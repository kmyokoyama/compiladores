#include "comp_dict.h"
#include "comp_tree.h"

/* Exit codes */

#define IKS_SUCCESS			0	/*<! No errors */

/* Variable declaration */
#define IKS_ERROR_UNDECLARED		1 	/*<! No declared ID */
#define IKS_ERROR_DECLARED		2	/*<! Already declared ID */

/* Identifiers use */
#define IKS_ERROR_VARIABLE		3 	/*<! ID must be used as variable */
#define IKS_ERROR_VECTOR		4	/*<! ID must be used as vector */
#define IKS_ERROR_FUNCTION		5	/*<! ID must be used as function */

/* Type and data size */
#define IKS_ERROR_WRONG_TYPE		6 	/*<! Incompatible type */
#define IKS_ERROR_STRING_TO_X		7	/*<! Impossible string conversion */
#define IKS_ERROR_CHAR_TO_X		8	/*<! Impossible char conversion */

/* Arguments and parameters */
#define IKS_ERROR_MISSING_ARGS	9 	/*<! Missing arguments */
#define IKS_ERROR_EXCESS_ARGS		10	/*<! Exceeding arguments */
#define IKS_ERROR_WRONG_TYPE_ARGS	11	/*<! Wrong arguments */

/* Commands type verfication */
#define IKS_ERROR_WRONG_PAR_INPUT	12	/*<! Parameter is not an ID */
#define IKS_ERROR_WRONG_PAR_OUTPUT	13	/*<! Parameter is not a string or expression */
#define IKS_ERROR_WRONG_PAR_RETURN	14	/*<! Parameter is not an return valid expression */


#define IKS_SIMBOLO_INT    1
#define IKS_SIMBOLO_FLOAT  2
#define IKS_SIMBOLO_CHAR   3
#define IKS_SIMBOLO_STRING 4
#define IKS_SIMBOLO_BOOL   5

#define CAST_INT_TO_FLOAT 1
#define CAST_INT_TO_BOOL 2
#define CAST_FLOAT_TO_INT 3
#define CAST_FLOAT_TO_BOOL 4
#define CAST_BOOL_TO_INT 5
#define CAST_BOOL_TO_FLOAT 6

/**
 * Verify operands and nodes types and implements inferences rules
 */
void verifyArithOperation(COMP_TREE_T* node); 

/**
 *	Verify if an ID declaration is right
 */
void verifyDeclaration(COMP_DICT_T* dict, char* key);

/**
 *	Verify if key is a valid vector
 */

void verifyVector(COMP_DICT_T* dict, char* key);

/**
 *	Verify if a function as all the right parameters using the created ast.
 */
void verifyFunctionCall(COMP_DICT_T* dict, char* key, COMP_TREE_T* parametersTree);

/**
 *	Verify if a ID was declared
 */

 void verifyID(COMP_DICT_T* dict, char* key);