/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 268 of yacc.c  */
#line 1 "parser.y"


/**
	@brief Grammar rules for IKS language.
	@file parser.y
	@author Alan Wink 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "comp_list.h"
#include "comp_dict.h"
#include "comp_tree.h"
#include "iks_ast.h"

#define DICTIONARY_HASH_SIZE 1000

COMP_TREE_T* root; /*< Root node for AST */

extern	COMP_DICT_T *scannerDict; 	/*< Scanner Dictionary reference 									*/
		COMP_DICT_T *globalDict; 	/*< Global Dictionary 											*/
		COMP_DICT_T *localDict; 	/*< Local Dictionary 											*/
		COMP_DICT_T *contextDict; 	/*< Context. Switch between local and global accordingly to the declaration type 	*/

COMP_DICT_ITEM_T* functionItem;

/**
 * Init global dict and the context
 */
 void initDict(){
 	globalDict = create_dictionary( DICTIONARY_HASH_SIZE, NULL);
 	localDict = NULL;
 	contextDict = globalDict;
 }

/**
 *	Switch for local variable context.
 */
 void chageToLocalContext(){
 	
 	//printf("\n \n GLOBAL: \n");printDict(globalDict);
 	/* Create a new dictionary */
 	localDict = create_dictionary( DICTIONARY_HASH_SIZE, globalDict);

 	/* Change context */
 	contextDict = localDict;
 }

 /**
  * 	Switch for global context.
  */
void changeToGlobalContext(){

	//printf("\n \n LOCAL: \n");printDict(localDict);
	/* Change context */
	contextDict = globalDict;
}

/**
 * Convert a char to string
 */

char* charToString(char target){
	char* finalString;
	
	finalString = malloc(sizeof(char)*2);
	finalString[0] = target;
	finalString[1] = '\0';

	return finalString;	
}

/**
 * Create a list item for function analisys.
 */
COMP_LIST_T* functionListAdd(COMP_LIST_T* list, char* key, int itemType){
	COMP_DICT_ITEM_T* newDictNode;
	newDictNode = malloc(sizeof(COMP_DICT_ITEM_T));

	newDictNode->key = strdup(key);
	newDictNode->itemType = itemType;
	newDictNode->isVector = 0;
	newDictNode->isFunction = 0;
	newDictNode->ready = 1;

	return insert_last(list,  newDictNode); /* As the list is created backwards, always add the last node in the first place to reorder the list. */

}



/* Line 268 of yacc.c  */
#line 165 "/home/alanwink/compilers/compilerproject/testscript/g1/parser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TK_PR_INT = 258,
     TK_PR_FLOAT = 259,
     TK_PR_BOOL = 260,
     TK_PR_CHAR = 261,
     TK_PR_STRING = 262,
     TK_PR_IF = 263,
     TK_PR_THEN = 264,
     TK_PR_ELSE = 265,
     TK_PR_WHILE = 266,
     TK_PR_DO = 267,
     TK_PR_INPUT = 268,
     TK_PR_OUTPUT = 269,
     TK_PR_RETURN = 270,
     TK_OC_LE = 271,
     TK_OC_GE = 272,
     TK_OC_EQ = 273,
     TK_OC_NE = 274,
     TK_OC_AND = 275,
     TK_OC_OR = 276,
     TK_LIT_INT = 277,
     TK_LIT_FLOAT = 278,
     TK_LIT_FALSE = 279,
     TK_LIT_TRUE = 280,
     TK_LIT_CHAR = 281,
     TK_LIT_STRING = 282,
     TK_IDENTIFICADOR = 283,
     TOKEN_ERRO = 284
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 293 of yacc.c  */
#line 95 "parser.y"

	COMP_DICT_ITEM_T *symbol;
	COMP_TREE_T* ast;
	COMP_LIST_T* list;
	int value;



/* Line 293 of yacc.c  */
#line 239 "/home/alanwink/compilers/compilerproject/testscript/g1/parser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 343 of yacc.c  */
#line 251 "/home/alanwink/compilers/compilerproject/testscript/g1/parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   586

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  47
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  35
/* YYNRULES -- Number of rules.  */
#define YYNRULES  113
/* YYNRULES -- Number of states.  */
#define YYNSTATES  198

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   284

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    31,     2,     2,     2,     2,     2,     2,
      42,    43,    36,    34,    44,    35,     2,    37,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    38,    39,
      32,    30,    33,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    40,     2,    41,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    45,     2,    46,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,     7,    10,    13,    14,    19,    27,
      29,    31,    33,    35,    37,    41,    42,    48,    52,    57,
      58,    64,    65,    71,    72,    76,    79,    82,    83,    85,
      87,    89,    91,    93,    95,    97,    98,   100,   102,   104,
     106,   108,   110,   112,   116,   120,   125,   127,   129,   131,
     133,   140,   149,   156,   163,   168,   171,   172,   176,   177,
     180,   183,   186,   188,   192,   195,   198,   202,   206,   210,
     214,   218,   222,   226,   230,   234,   238,   242,   246,   248,
     250,   252,   254,   256,   258,   260,   263,   267,   270,   272,
     274,   278,   282,   286,   290,   294,   298,   302,   306,   310,
     314,   318,   322,   324,   326,   328,   330,   332,   334,   336,
     339,   343,   346,   348
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      48,     0,    -1,    49,    50,    -1,    -1,    51,    50,    -1,
      53,    50,    -1,    -1,    52,    38,    28,    39,    -1,    52,
      38,    28,    40,    22,    41,    39,    -1,     3,    -1,     4,
      -1,     6,    -1,     5,    -1,     7,    -1,    54,    59,    60,
      -1,    -1,    52,    38,    28,    55,    56,    -1,    42,    57,
      43,    -1,    52,    38,    28,    58,    -1,    -1,    44,    52,
      38,    28,    58,    -1,    -1,    52,    38,    28,    39,    59,
      -1,    -1,    45,    61,    46,    -1,    63,    62,    -1,    39,
      61,    -1,    -1,    60,    -1,    65,    -1,    67,    -1,    72,
      -1,    75,    -1,    76,    -1,    79,    -1,    -1,    60,    -1,
      65,    -1,    67,    -1,    72,    -1,    75,    -1,    76,    -1,
      79,    -1,    28,    30,    80,    -1,    66,    30,    80,    -1,
      28,    40,    80,    41,    -1,    68,    -1,    69,    -1,    70,
      -1,    71,    -1,     8,    42,    80,    43,     9,    64,    -1,
       8,    42,    80,    43,     9,    64,    10,    64,    -1,    11,
      42,    80,    43,    12,    64,    -1,    12,    64,    11,    42,
      80,    43,    -1,    28,    42,    73,    43,    -1,    80,    74,
      -1,    -1,    44,    80,    74,    -1,    -1,    13,    28,    -1,
      14,    77,    -1,    80,    78,    -1,    80,    -1,    44,    80,
      78,    -1,    44,    80,    -1,    15,    80,    -1,    81,    34,
      81,    -1,    81,    35,    81,    -1,    81,    36,    81,    -1,
      81,    37,    81,    -1,    81,    18,    81,    -1,    81,    19,
      81,    -1,    81,    20,    81,    -1,    81,    21,    81,    -1,
      81,    32,    81,    -1,    81,    33,    81,    -1,    81,    16,
      81,    -1,    81,    17,    81,    -1,    28,    -1,    25,    -1,
      24,    -1,    22,    -1,    23,    -1,    26,    -1,    27,    -1,
      31,    81,    -1,    42,    81,    43,    -1,    35,    81,    -1,
      66,    -1,    72,    -1,    81,    34,    81,    -1,    81,    35,
      81,    -1,    81,    36,    81,    -1,    81,    37,    81,    -1,
      81,    18,    81,    -1,    81,    19,    81,    -1,    81,    20,
      81,    -1,    81,    21,    81,    -1,    81,    32,    81,    -1,
      81,    33,    81,    -1,    81,    16,    81,    -1,    81,    17,
      81,    -1,    28,    -1,    25,    -1,    24,    -1,    22,    -1,
      23,    -1,    26,    -1,    27,    -1,    31,    81,    -1,    42,
      81,    43,    -1,    35,    81,    -1,    66,    -1,    72,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   201,   201,   205,   209,   210,   211,   216,   225,   238,
     239,   240,   241,   242,   247,   253,   252,   278,   282,   289,
     293,   300,   305,   315,   320,   325,   330,   331,   336,   337,
     338,   339,   340,   341,   342,   343,   348,   349,   350,   351,
     352,   353,   354,   359,   369,   376,   391,   392,   393,   394,
     398,   405,   412,   419,   426,   441,   442,   446,   447,   452,
     467,   471,   472,   476,   477,   482,   487,   488,   489,   490,
     491,   492,   493,   494,   495,   496,   497,   498,   499,   500,
     501,   502,   503,   504,   505,   506,   507,   508,   509,   510,
     514,   515,   516,   517,   518,   519,   520,   521,   522,   523,
     524,   525,   526,   527,   528,   529,   530,   531,   532,   533,
     534,   535,   536,   537
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TK_PR_INT", "TK_PR_FLOAT", "TK_PR_BOOL",
  "TK_PR_CHAR", "TK_PR_STRING", "TK_PR_IF", "TK_PR_THEN", "TK_PR_ELSE",
  "TK_PR_WHILE", "TK_PR_DO", "TK_PR_INPUT", "TK_PR_OUTPUT", "TK_PR_RETURN",
  "TK_OC_LE", "TK_OC_GE", "TK_OC_EQ", "TK_OC_NE", "TK_OC_AND", "TK_OC_OR",
  "TK_LIT_INT", "TK_LIT_FLOAT", "TK_LIT_FALSE", "TK_LIT_TRUE",
  "TK_LIT_CHAR", "TK_LIT_STRING", "TK_IDENTIFICADOR", "TOKEN_ERRO", "'='",
  "'!'", "'<'", "'>'", "'+'", "'-'", "'*'", "'/'", "':'", "';'", "'['",
  "']'", "'('", "')'", "','", "'{'", "'}'", "$accept", "iksProgram",
  "iksBegin", "globalDeclaration", "variableDeclaration",
  "typeDeclaration", "functionDeclaration", "functionHeader", "$@1",
  "functionHeaderList", "functionHeaderListItemFirst",
  "functionHeaderListItemRest", "functionVariables", "functionBody",
  "functionBodyCommandList", "functionCommandNext", "functionCommand",
  "functionCommandNoEmpty", "functionCommandAttribution", "vectorIndexing",
  "functionCommandFlowControl", "ifCommand", "ifThenElseCommand",
  "whileDoCommand", "doWhileCommand", "functionCommandFunctionCall",
  "functionCommandFunctionCallParameterListFirst",
  "functionCommandFunctionCallParameterListRest", "functionCommandInput",
  "functionCommandOutput", "functionCommandOutputListFirst",
  "functionCommandOutputListRest", "functionCommandReturn", "expression",
  "expressionCompound", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
      61,    33,    60,    62,    43,    45,    42,    47,    58,    59,
      91,    93,    40,    41,    44,   123,   125
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    47,    48,    49,    50,    50,    50,    51,    51,    52,
      52,    52,    52,    52,    53,    55,    54,    56,    57,    57,
      58,    58,    59,    59,    60,    61,    62,    62,    63,    63,
      63,    63,    63,    63,    63,    63,    64,    64,    64,    64,
      64,    64,    64,    65,    65,    66,    67,    67,    67,    67,
      68,    69,    70,    71,    72,    73,    73,    74,    74,    75,
      76,    77,    77,    78,    78,    79,    80,    80,    80,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      81,    81,    81,    81,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    81
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     0,     2,     2,     0,     4,     7,     1,
       1,     1,     1,     1,     3,     0,     5,     3,     4,     0,
       5,     0,     5,     0,     3,     2,     2,     0,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     4,     1,     1,     1,     1,
       6,     8,     6,     6,     4,     2,     0,     3,     0,     2,
       2,     2,     1,     3,     2,     2,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     1,     1,
       1,     1,     1,     1,     1,     2,     3,     2,     1,     1,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     1,     1,     1,     1,     1,     1,     1,     2,
       3,     2,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     6,     1,     9,    10,    12,    11,    13,     2,
       6,     0,     6,    23,     4,     0,     5,     0,     0,    15,
       0,    35,    14,     7,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    28,     0,    27,    29,     0,    30,
      46,    47,    48,    49,    31,    32,    33,    34,     0,    19,
      16,    23,     0,     0,    36,     0,    37,    38,    39,    40,
      41,    42,    59,   105,   106,   104,   103,   107,   108,   102,
       0,     0,     0,   112,   113,    60,    62,     0,    65,     0,
       0,    56,    24,    35,    25,     0,     0,     0,     0,    22,
       0,     0,     0,   105,   106,   104,   103,   107,   108,   102,
       0,     0,     0,   112,   113,    85,   111,     0,     0,    61,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    43,     0,     0,    58,    26,    44,     8,     0,
      17,     0,     0,     0,   109,   111,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   110,
      64,    76,    77,    70,    71,    72,    73,    74,    75,    90,
      91,    92,    93,    45,    54,     0,    55,    21,     0,     0,
       0,   110,   100,   101,    94,    95,    96,    97,    98,    99,
      90,    91,    92,    93,    63,    58,     0,    18,    50,    52,
      53,    57,     0,     0,     0,    51,    21,    20
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,     9,    10,    11,    12,    13,    25,    50,
      88,   187,    18,    54,    35,    84,    36,    55,    56,   103,
      57,    40,    41,    42,    43,   104,   124,   166,    59,    60,
      75,   109,    61,    76,    77
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -162
static const yytype_int16 yypact[] =
{
    -162,     5,    52,  -162,  -162,  -162,  -162,  -162,  -162,  -162,
      52,   -23,    52,    52,  -162,    11,  -162,    32,   -20,    12,
      35,    79,  -162,  -162,    50,    37,    42,    46,    53,    79,
      56,   316,   316,    70,  -162,    55,    60,  -162,    73,  -162,
    -162,  -162,  -162,  -162,  -162,  -162,  -162,  -162,    63,    52,
    -162,    52,   316,   316,  -162,    94,  -162,  -162,  -162,  -162,
    -162,  -162,  -162,   200,   206,   212,   227,   249,   264,    87,
     544,   544,   544,   270,   276,  -162,    62,   382,  -162,   316,
     316,   316,  -162,    79,  -162,   316,    78,    81,    77,  -162,
      80,    82,    90,  -162,  -162,  -162,  -162,  -162,  -162,   -36,
     544,   544,   544,  -162,  -162,   388,    72,    10,   316,  -162,
     544,   544,   544,   544,   544,   544,   544,   544,   544,   544,
     544,   544,  -162,    96,    95,    97,  -162,  -162,  -162,   111,
    -162,   113,   128,   316,   528,    38,   354,   544,   544,   544,
     544,   544,   544,   544,   544,   544,   544,   544,   544,   286,
      62,   410,   416,   438,   444,   466,   472,   494,   500,   125,
     141,   313,   323,  -162,  -162,   316,  -162,    98,    79,    79,
     100,  -162,   528,   528,   528,   528,   522,   472,   528,   528,
      38,    38,  -162,  -162,  -162,    97,    52,  -162,   134,  -162,
    -162,  -162,   107,    79,   119,  -162,    98,  -162
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -162,  -162,  -162,    25,  -162,   -13,  -162,  -162,  -162,  -162,
    -162,   -48,   103,    -7,    84,  -162,  -162,  -161,   -18,   -19,
      -5,  -162,  -162,  -162,  -162,   -12,  -162,   -30,    -3,     2,
    -162,    13,     3,   -31,    88
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -110
static const yytype_int16 yytable[] =
{
      17,    78,    38,    37,    80,     3,    81,   188,   189,    44,
      38,    22,    73,    73,    34,    15,    39,    58,    45,    74,
      74,    90,    91,    46,    47,    21,   137,   138,   139,   140,
     141,   142,   195,    73,    73,    14,    87,    16,    17,    19,
      74,    74,   143,   144,   145,   146,   147,   148,   122,   123,
     125,    23,    24,   149,   127,     4,     5,     6,     7,     8,
      73,    73,    73,    26,    38,    37,    73,    74,    74,    74,
      20,    44,    48,    74,   147,   148,    34,   150,    39,    49,
      45,    51,   -87,   -87,    62,    46,    47,    27,    52,    73,
      28,    29,    30,    31,    32,    53,    74,   -78,   -78,    83,
      79,    82,   170,    85,    86,    92,   108,    33,   147,   148,
      80,   -87,    81,   -87,    73,   -87,   -87,   128,   -87,   129,
     130,    74,   168,   131,    21,   132,   -78,    80,   -78,    81,
     -78,   -78,   133,   -78,   185,   -66,   -66,   163,   164,   167,
     169,   165,   186,   190,   193,   194,    73,   196,   197,    38,
      38,   -67,   -67,    74,    89,   191,    58,    58,   105,   106,
     107,   147,   148,   184,   -66,     0,   -66,   126,   -66,   -66,
       0,   -66,     0,   192,    38,     0,     0,   147,   148,     0,
     -67,    58,   -67,     0,   -67,   -67,     0,   -67,   134,   135,
     136,     0,     0,     0,     0,     0,     0,     0,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     -81,   -81,     0,     0,     0,     0,   -82,   -82,     0,     0,
       0,     0,   -80,   -80,     0,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   -79,   -79,   -81,
       0,   -81,     0,   -81,   -81,   -82,   -81,   -82,     0,   -82,
     -82,   -80,   -82,   -80,     0,   -80,   -80,     0,   -80,   -83,
     -83,     0,     0,     0,     0,     0,   -79,     0,   -79,     0,
     -79,   -79,     0,   -79,   -84,   -84,     0,     0,     0,     0,
     -88,   -88,     0,     0,     0,     0,   -89,   -89,   -83,     0,
     -83,     0,   -83,   -83,     0,   -83,   -86,   -86,     0,     0,
       0,     0,     0,   -84,     0,   -84,     0,   -84,   -84,   -88,
     -84,   -88,     0,   -88,   -88,   -89,   -88,   -89,     0,   -89,
     -89,     0,   -89,   -68,   -68,   -86,     0,   -86,     0,   -86,
     -86,     0,   -86,   -69,   -69,     0,     0,     0,    63,    64,
      65,    66,    67,    68,    69,     0,     0,    70,     0,     0,
       0,    71,   -68,     0,   -68,     0,   -68,   -68,    72,   -68,
       0,     0,   -69,     0,   -69,     0,   -69,   -69,     0,   -69,
     137,   138,   139,   140,   141,   142,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   143,   144,   145,   146,
     147,   148,     0,     0,     0,     0,     0,   171,   110,   111,
     112,   113,   114,   115,  -110,  -110,  -110,  -110,  -109,  -109,
       0,     0,     0,     0,   116,   117,   118,   119,   120,   121,
    -110,  -110,   145,   146,   147,   148,  -110,  -110,  -110,  -110,
    -100,  -100,  -110,  -110,  -110,  -110,  -101,  -101,     0,     0,
       0,     0,  -110,  -110,   145,   146,   147,   148,  -110,  -110,
     145,   146,   147,   148,  -110,  -110,  -110,  -110,   -94,   -94,
    -110,  -110,  -110,  -110,   -95,   -95,     0,     0,     0,     0,
    -110,  -110,   145,   146,   147,   148,  -110,  -110,   145,   146,
     147,   148,   137,   138,   139,   140,   141,   -96,   137,   138,
     139,   140,   141,   142,     0,     0,     0,     0,   143,   144,
     145,   146,   147,   148,   143,   144,   145,   146,   147,   148,
    -110,  -110,  -110,  -110,   -98,   -98,  -110,  -110,  -110,  -110,
     -99,   -99,     0,     0,     0,     0,  -110,  -110,   145,   146,
     147,   148,  -110,  -110,   145,   146,   147,   148,   137,   138,
     139,   140,   141,     0,  -110,  -110,  -110,  -110,     0,     0,
       0,     0,     0,     0,   143,   144,   145,   146,   147,   148,
    -110,  -110,   145,   146,   147,   148,    93,    94,    95,    96,
      97,    98,    99,     0,     0,   100,     0,     0,     0,   101,
       0,     0,     0,     0,     0,     0,   102
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-162))

#define yytable_value_is_error(yytable_value) \
  ((yytable_value) == (-110))

static const yytype_int16 yycheck[] =
{
      13,    32,    21,    21,    40,     0,    42,   168,   169,    21,
      29,    18,    31,    32,    21,    38,    21,    29,    21,    31,
      32,    52,    53,    21,    21,    45,    16,    17,    18,    19,
      20,    21,   193,    52,    53,    10,    49,    12,    51,    28,
      52,    53,    32,    33,    34,    35,    36,    37,    79,    80,
      81,    39,    40,    43,    85,     3,     4,     5,     6,     7,
      79,    80,    81,    28,    83,    83,    85,    79,    80,    81,
      38,    83,    22,    85,    36,    37,    83,   108,    83,    42,
      83,    39,    10,    11,    28,    83,    83,     8,    42,   108,
      11,    12,    13,    14,    15,    42,   108,    10,    11,    39,
      30,    46,   133,    30,    41,    11,    44,    28,    36,    37,
      40,    39,    42,    41,   133,    43,    44,    39,    46,    38,
      43,   133,     9,    43,    45,    43,    39,    40,    41,    42,
      43,    44,    42,    46,   165,    10,    11,    41,    43,    28,
      12,    44,    44,    43,    10,    38,   165,    28,   196,   168,
     169,    10,    11,   165,    51,   185,   168,   169,    70,    71,
      72,    36,    37,   150,    39,    -1,    41,    83,    43,    44,
      -1,    46,    -1,   186,   193,    -1,    -1,    36,    37,    -1,
      39,   193,    41,    -1,    43,    44,    -1,    46,   100,   101,
     102,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
      10,    11,    -1,    -1,    -1,    -1,    10,    11,    -1,    -1,
      -1,    -1,    10,    11,    -1,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,    10,    11,    39,
      -1,    41,    -1,    43,    44,    39,    46,    41,    -1,    43,
      44,    39,    46,    41,    -1,    43,    44,    -1,    46,    10,
      11,    -1,    -1,    -1,    -1,    -1,    39,    -1,    41,    -1,
      43,    44,    -1,    46,    10,    11,    -1,    -1,    -1,    -1,
      10,    11,    -1,    -1,    -1,    -1,    10,    11,    39,    -1,
      41,    -1,    43,    44,    -1,    46,    10,    11,    -1,    -1,
      -1,    -1,    -1,    39,    -1,    41,    -1,    43,    44,    39,
      46,    41,    -1,    43,    44,    39,    46,    41,    -1,    43,
      44,    -1,    46,    10,    11,    39,    -1,    41,    -1,    43,
      44,    -1,    46,    10,    11,    -1,    -1,    -1,    22,    23,
      24,    25,    26,    27,    28,    -1,    -1,    31,    -1,    -1,
      -1,    35,    39,    -1,    41,    -1,    43,    44,    42,    46,
      -1,    -1,    39,    -1,    41,    -1,    43,    44,    -1,    46,
      16,    17,    18,    19,    20,    21,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    32,    33,    34,    35,
      36,    37,    -1,    -1,    -1,    -1,    -1,    43,    16,    17,
      18,    19,    20,    21,    16,    17,    18,    19,    20,    21,
      -1,    -1,    -1,    -1,    32,    33,    34,    35,    36,    37,
      32,    33,    34,    35,    36,    37,    16,    17,    18,    19,
      20,    21,    16,    17,    18,    19,    20,    21,    -1,    -1,
      -1,    -1,    32,    33,    34,    35,    36,    37,    32,    33,
      34,    35,    36,    37,    16,    17,    18,    19,    20,    21,
      16,    17,    18,    19,    20,    21,    -1,    -1,    -1,    -1,
      32,    33,    34,    35,    36,    37,    32,    33,    34,    35,
      36,    37,    16,    17,    18,    19,    20,    21,    16,    17,
      18,    19,    20,    21,    -1,    -1,    -1,    -1,    32,    33,
      34,    35,    36,    37,    32,    33,    34,    35,    36,    37,
      16,    17,    18,    19,    20,    21,    16,    17,    18,    19,
      20,    21,    -1,    -1,    -1,    -1,    32,    33,    34,    35,
      36,    37,    32,    33,    34,    35,    36,    37,    16,    17,
      18,    19,    20,    -1,    16,    17,    18,    19,    -1,    -1,
      -1,    -1,    -1,    -1,    32,    33,    34,    35,    36,    37,
      32,    33,    34,    35,    36,    37,    22,    23,    24,    25,
      26,    27,    28,    -1,    -1,    31,    -1,    -1,    -1,    35,
      -1,    -1,    -1,    -1,    -1,    -1,    42
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    48,    49,     0,     3,     4,     5,     6,     7,    50,
      51,    52,    53,    54,    50,    38,    50,    52,    59,    28,
      38,    45,    60,    39,    40,    55,    28,     8,    11,    12,
      13,    14,    15,    28,    60,    61,    63,    65,    66,    67,
      68,    69,    70,    71,    72,    75,    76,    79,    22,    42,
      56,    39,    42,    42,    60,    64,    65,    67,    72,    75,
      76,    79,    28,    22,    23,    24,    25,    26,    27,    28,
      31,    35,    42,    66,    72,    77,    80,    81,    80,    30,
      40,    42,    46,    39,    62,    30,    41,    52,    57,    59,
      80,    80,    11,    22,    23,    24,    25,    26,    27,    28,
      31,    35,    42,    66,    72,    81,    81,    81,    44,    78,
      16,    17,    18,    19,    20,    21,    32,    33,    34,    35,
      36,    37,    80,    80,    73,    80,    61,    80,    39,    38,
      43,    43,    43,    42,    81,    81,    81,    16,    17,    18,
      19,    20,    21,    32,    33,    34,    35,    36,    37,    43,
      80,    81,    81,    81,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    41,    43,    44,    74,    28,     9,    12,
      80,    43,    81,    81,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    78,    80,    44,    58,    64,    64,
      43,    74,    52,    10,    38,    64,    28,    58
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1806 of yacc.c  */
#line 201 "parser.y"
    {root = create_ast_node(IKS_AST_PROGRAMA, NULL, NULL, 1, (yyvsp[(2) - (2)].ast));}
    break;

  case 3:

/* Line 1806 of yacc.c  */
#line 205 "parser.y"
    {initDict();}
    break;

  case 4:

/* Line 1806 of yacc.c  */
#line 209 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (2)].ast);							}
    break;

  case 5:

/* Line 1806 of yacc.c  */
#line 210 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(1) - (2)].ast), 1, (yyvsp[(2) - (2)].ast));			}
    break;

  case 6:

/* Line 1806 of yacc.c  */
#line 211 "parser.y"
    {(yyval.ast) = NULL;							}
    break;

  case 7:

/* Line 1806 of yacc.c  */
#line 216 "parser.y"
    { 	change_dictionary_item_itemValue(scannerDict, (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(1) - (4)].value));
														verifyDeclaration(contextDict, (yyvsp[(3) - (4)].symbol)->key);
														COMP_DICT_ITEM_T* newItem;
														newItem = insert_dictionary_item(contextDict, (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(1) - (4)].value), (yyvsp[(3) - (4)].symbol)->lineNumber);
														newItem->parameterList = NULL;
														newItem->isVector = 0;
														newItem->isFunction = 0;
														newItem->ready = 1;
													}
    break;

  case 8:

/* Line 1806 of yacc.c  */
#line 226 "parser.y"
    { 	change_dictionary_item_itemValue(scannerDict, (yyvsp[(3) - (7)].symbol)->key, (yyvsp[(1) - (7)].value));
													verifyDeclaration(contextDict, (yyvsp[(3) - (7)].symbol)->key);
													COMP_DICT_ITEM_T* newItem;
													newItem = insert_dictionary_item(contextDict, (yyvsp[(3) - (7)].symbol)->key, (yyvsp[(1) - (7)].value), (yyvsp[(3) - (7)].symbol)->lineNumber);
													newItem->parameterList = NULL;
													newItem->isVector = 1;
													newItem->isFunction = 0;
													newItem->ready = 1;
												}
    break;

  case 9:

/* Line 1806 of yacc.c  */
#line 238 "parser.y"
    {(yyval.value) = IKS_SIMBOLO_INT; 		}
    break;

  case 10:

/* Line 1806 of yacc.c  */
#line 239 "parser.y"
    {(yyval.value) = IKS_SIMBOLO_FLOAT; 	}
    break;

  case 11:

/* Line 1806 of yacc.c  */
#line 240 "parser.y"
    {(yyval.value) = IKS_SIMBOLO_CHAR; 	}
    break;

  case 12:

/* Line 1806 of yacc.c  */
#line 241 "parser.y"
    {(yyval.value) = IKS_SIMBOLO_BOOL; 	}
    break;

  case 13:

/* Line 1806 of yacc.c  */
#line 242 "parser.y"
    {(yyval.value) = IKS_SIMBOLO_STRING; 	}
    break;

  case 14:

/* Line 1806 of yacc.c  */
#line 247 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(1) - (3)].ast), 1, (yyvsp[(3) - (3)].ast));changeToGlobalContext();/* End of function */}
    break;

  case 15:

/* Line 1806 of yacc.c  */
#line 253 "parser.y"
    {
				verifyDeclaration(contextDict, (yyvsp[(3) - (3)].symbol)->key);
				insert_dictionary_item(contextDict, (yyvsp[(3) - (3)].symbol)->key, (yyvsp[(3) - (3)].symbol)->itemType, (yyvsp[(3) - (3)].symbol)->lineNumber);
				change_dictionary_item_itemValue(contextDict, (yyvsp[(3) - (3)].symbol)->key, (yyvsp[(1) - (3)].value));
				
				functionItem = get_dictionary_item(contextDict, (yyvsp[(3) - (3)].symbol)->key);
				functionItem->isVector = 0;
				functionItem->isFunction = 1;
				functionItem->ready = 0;
				
				
				chageToLocalContext();

			}
    break;

  case 16:

/* Line 1806 of yacc.c  */
#line 268 "parser.y"
    {	
				functionItem = get_dictionary_item(globalDict, (yyvsp[(3) - (5)].symbol)->key); /* Function declaration is in global */
				functionItem->parameterList = (yyvsp[(5) - (5)].list);
				functionItem->ready = 1;
				(yyval.ast) = create_ast_node(IKS_AST_FUNCAO, NULL, (yyvsp[(3) - (5)].symbol)->key, 0);
			}
    break;

  case 17:

/* Line 1806 of yacc.c  */
#line 278 "parser.y"
    {(yyval.list) = (yyvsp[(2) - (3)].list);}
    break;

  case 18:

/* Line 1806 of yacc.c  */
#line 282 "parser.y"
    { 	
													change_dictionary_item_itemValue(scannerDict, (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(1) - (4)].value));
													verifyDeclaration(contextDict, (yyvsp[(3) - (4)].symbol)->key);
													insert_dictionary_item(contextDict, (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(3) - (4)].symbol)->itemType, (yyvsp[(3) - (4)].symbol)->lineNumber);
													change_dictionary_item_itemValue(contextDict, (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(1) - (4)].value));
													(yyval.list) = functionListAdd((yyvsp[(4) - (4)].list), (yyvsp[(3) - (4)].symbol)->key, (yyvsp[(1) - (4)].value)); /* Create a list item for functions */
												}
    break;

  case 19:

/* Line 1806 of yacc.c  */
#line 289 "parser.y"
    {	(yyval.list) = NULL;	}
    break;

  case 20:

/* Line 1806 of yacc.c  */
#line 293 "parser.y"
    { 	
												 		change_dictionary_item_itemValue(scannerDict, (yyvsp[(4) - (5)].symbol)->key, (yyvsp[(2) - (5)].value));
												 		verifyDeclaration(contextDict, (yyvsp[(4) - (5)].symbol)->key);
												 		insert_dictionary_item(contextDict, (yyvsp[(4) - (5)].symbol)->key, (yyvsp[(4) - (5)].symbol)->itemType, (yyvsp[(4) - (5)].symbol)->lineNumber);
														change_dictionary_item_itemValue(contextDict, (yyvsp[(4) - (5)].symbol)->key, (yyvsp[(2) - (5)].value));
														(yyval.list) = functionListAdd((yyvsp[(5) - (5)].list), (yyvsp[(4) - (5)].symbol)->key, (yyvsp[(2) - (5)].value)); /* Create a list item for functions */
													}
    break;

  case 21:

/* Line 1806 of yacc.c  */
#line 300 "parser.y"
    { (yyval.list) = NULL;	}
    break;

  case 22:

/* Line 1806 of yacc.c  */
#line 305 "parser.y"
    { 	
													change_dictionary_item_itemValue(scannerDict, (yyvsp[(3) - (5)].symbol)->key, (yyvsp[(1) - (5)].value));
													verifyDeclaration(contextDict, (yyvsp[(3) - (5)].symbol)->key);
													COMP_DICT_ITEM_T* newItem;
													newItem = insert_dictionary_item(contextDict, (yyvsp[(3) - (5)].symbol)->key, (yyvsp[(1) - (5)].value), (yyvsp[(3) - (5)].symbol)->lineNumber);
													newItem->parameterList = NULL;
													newItem->isVector = 0;
													newItem->isFunction = 0;
													newItem->ready = 1;
												}
    break;

  case 24:

/* Line 1806 of yacc.c  */
#line 320 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (3)].ast); }
    break;

  case 25:

/* Line 1806 of yacc.c  */
#line 325 "parser.y"
    { (yyval.ast) = add_son_ast_node((yyvsp[(1) - (2)].ast), 1, (yyvsp[(2) - (2)].ast)); }
    break;

  case 26:

/* Line 1806 of yacc.c  */
#line 330 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (2)].ast);	}
    break;

  case 27:

/* Line 1806 of yacc.c  */
#line 331 "parser.y"
    {(yyval.ast) = NULL;	}
    break;

  case 28:

/* Line 1806 of yacc.c  */
#line 336 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_BLOCO, NULL, NULL, 1, (yyvsp[(1) - (1)].ast));	}
    break;

  case 29:

/* Line 1806 of yacc.c  */
#line 337 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 30:

/* Line 1806 of yacc.c  */
#line 338 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 31:

/* Line 1806 of yacc.c  */
#line 339 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 32:

/* Line 1806 of yacc.c  */
#line 340 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 33:

/* Line 1806 of yacc.c  */
#line 341 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 34:

/* Line 1806 of yacc.c  */
#line 342 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 35:

/* Line 1806 of yacc.c  */
#line 343 "parser.y"
    {(yyval.ast) = NULL;									}
    break;

  case 36:

/* Line 1806 of yacc.c  */
#line 348 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_BLOCO, NULL, NULL, 1, (yyvsp[(1) - (1)].ast));	}
    break;

  case 37:

/* Line 1806 of yacc.c  */
#line 349 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 38:

/* Line 1806 of yacc.c  */
#line 350 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 39:

/* Line 1806 of yacc.c  */
#line 351 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 40:

/* Line 1806 of yacc.c  */
#line 352 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 41:

/* Line 1806 of yacc.c  */
#line 353 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 42:

/* Line 1806 of yacc.c  */
#line 354 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);									}
    break;

  case 43:

/* Line 1806 of yacc.c  */
#line 359 "parser.y"
    {	verifyID(contextDict, (yyvsp[(1) - (3)].symbol)->key);
											(yyval.ast) = create_ast_node(
												IKS_AST_ATRIBUICAO,
												NULL,
												NULL, 
												2,	
												create_ast_node(IKS_AST_IDENTIFICADOR, (yyvsp[(1) - (3)].symbol), (yyvsp[(1) - (3)].symbol)->key, 0),
												(yyvsp[(3) - (3)].ast)
								     			); 
							}
    break;

  case 44:

/* Line 1806 of yacc.c  */
#line 369 "parser.y"
    { 
								(yyval.ast) = create_ast_node( IKS_AST_ATRIBUICAO, NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));	
							}
    break;

  case 45:

/* Line 1806 of yacc.c  */
#line 376 "parser.y"
    {
								verifyVector(globalDict, (yyvsp[(1) - (4)].symbol)->key); // There are only vectors in global context
								(yyval.ast) = create_ast_node(
										IKS_AST_VETOR_INDEXADO,
										NULL,
										NULL, 
										2,	
										create_ast_node(IKS_AST_IDENTIFICADOR, (yyvsp[(1) - (4)].symbol), (yyvsp[(1) - (4)].symbol)->key, 0),
										(yyvsp[(3) - (4)].ast)
									); 
							}
    break;

  case 46:

/* Line 1806 of yacc.c  */
#line 391 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);}
    break;

  case 47:

/* Line 1806 of yacc.c  */
#line 392 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);}
    break;

  case 48:

/* Line 1806 of yacc.c  */
#line 393 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);}
    break;

  case 49:

/* Line 1806 of yacc.c  */
#line 394 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);}
    break;

  case 50:

/* Line 1806 of yacc.c  */
#line 399 "parser.y"
    {
			(yyval.ast) = create_ast_node(IKS_AST_IF_ELSE, NULL, NULL, 2, (yyvsp[(3) - (6)].ast), (yyvsp[(6) - (6)].ast));
		}
    break;

  case 51:

/* Line 1806 of yacc.c  */
#line 406 "parser.y"
    {
			(yyval.ast) = create_ast_node(IKS_AST_IF_ELSE, NULL, NULL, 3, (yyvsp[(3) - (8)].ast), (yyvsp[(6) - (8)].ast), (yyvsp[(8) - (8)].ast));
		}
    break;

  case 52:

/* Line 1806 of yacc.c  */
#line 413 "parser.y"
    {
			(yyval.ast) = create_ast_node(IKS_AST_WHILE_DO, NULL, NULL, 2, (yyvsp[(3) - (6)].ast), (yyvsp[(6) - (6)].ast));
		}
    break;

  case 53:

/* Line 1806 of yacc.c  */
#line 420 "parser.y"
    {
			(yyval.ast) = create_ast_node(IKS_AST_DO_WHILE , NULL, NULL, 2, (yyvsp[(2) - (6)].ast), (yyvsp[(5) - (6)].ast));
		}
    break;

  case 54:

/* Line 1806 of yacc.c  */
#line 427 "parser.y"
    {
			//verifyFunctionCall(contextDict, $1, $3);
			(yyval.ast) = create_ast_node(
						IKS_AST_CHAMADA_DE_FUNCAO ,
						NULL, 
						NULL, 
						2, 
						create_ast_node(IKS_AST_IDENTIFICADOR, (yyvsp[(1) - (4)].symbol), (yyvsp[(1) - (4)].symbol)->key, 0), 
						(yyvsp[(3) - (4)].ast)
					); 
		}
    break;

  case 55:

/* Line 1806 of yacc.c  */
#line 441 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(1) - (2)].ast), 1, (yyvsp[(2) - (2)].ast));	}
    break;

  case 56:

/* Line 1806 of yacc.c  */
#line 442 "parser.y"
    {(yyval.ast) = NULL;					}
    break;

  case 57:

/* Line 1806 of yacc.c  */
#line 446 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(2) - (3)].ast), 1, (yyvsp[(3) - (3)].ast));	}
    break;

  case 58:

/* Line 1806 of yacc.c  */
#line 447 "parser.y"
    {(yyval.ast) = NULL;					}
    break;

  case 59:

/* Line 1806 of yacc.c  */
#line 453 "parser.y"
    {
			verifyID(contextDict, (yyvsp[(2) - (2)].symbol)->key);
			(yyval.ast) = create_ast_node(
						IKS_AST_INPUT , 
						NULL, 
						NULL, 
						1, 
						create_ast_node(IKS_AST_IDENTIFICADOR, (yyvsp[(2) - (2)].symbol), (yyvsp[(2) - (2)].symbol)->key, 0)
					);
		}
    break;

  case 60:

/* Line 1806 of yacc.c  */
#line 467 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_OUTPUT, NULL, NULL,1,  (yyvsp[(2) - (2)].ast));}
    break;

  case 61:

/* Line 1806 of yacc.c  */
#line 471 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(1) - (2)].ast), 1, (yyvsp[(2) - (2)].ast));	}
    break;

  case 62:

/* Line 1806 of yacc.c  */
#line 472 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast); 					}
    break;

  case 63:

/* Line 1806 of yacc.c  */
#line 476 "parser.y"
    {(yyval.ast) = add_son_ast_node((yyvsp[(2) - (3)].ast), 1, (yyvsp[(3) - (3)].ast));	}
    break;

  case 64:

/* Line 1806 of yacc.c  */
#line 477 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (2)].ast); 					}
    break;

  case 65:

/* Line 1806 of yacc.c  */
#line 482 "parser.y"
    {(yyval.ast) = create_ast_node( IKS_AST_RETURN , NULL, NULL, 1,(yyvsp[(2) - (2)].ast));}
    break;

  case 66:

/* Line 1806 of yacc.c  */
#line 487 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_SOMA, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 67:

/* Line 1806 of yacc.c  */
#line 488 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_SUBTRACAO, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 68:

/* Line 1806 of yacc.c  */
#line 489 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_MULTIPLICACAO, 	NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 69:

/* Line 1806 of yacc.c  */
#line 490 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_DIVISAO, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 70:

/* Line 1806 of yacc.c  */
#line 491 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_IGUAL, 	NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 71:

/* Line 1806 of yacc.c  */
#line 492 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_DIF, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 72:

/* Line 1806 of yacc.c  */
#line 493 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_E, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 73:

/* Line 1806 of yacc.c  */
#line 494 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_OU, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 74:

/* Line 1806 of yacc.c  */
#line 495 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_L, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 75:

/* Line 1806 of yacc.c  */
#line 496 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_G, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 76:

/* Line 1806 of yacc.c  */
#line 497 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_LE, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 77:

/* Line 1806 of yacc.c  */
#line 498 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_GE, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 78:

/* Line 1806 of yacc.c  */
#line 499 "parser.y"
    { verifyID(contextDict, (yyvsp[(1) - (1)].symbol)->key); (yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 79:

/* Line 1806 of yacc.c  */
#line 500 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 80:

/* Line 1806 of yacc.c  */
#line 501 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 81:

/* Line 1806 of yacc.c  */
#line 502 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 82:

/* Line 1806 of yacc.c  */
#line 503 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 83:

/* Line 1806 of yacc.c  */
#line 504 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), charToString((yyvsp[(1) - (1)].symbol)->charValue), 0);	}
    break;

  case 84:

/* Line 1806 of yacc.c  */
#line 505 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->stringValue, 0);			}
    break;

  case 85:

/* Line 1806 of yacc.c  */
#line 506 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_NEGACAO, 	NULL, NULL, 1, (yyvsp[(2) - (2)].ast));				}
    break;

  case 86:

/* Line 1806 of yacc.c  */
#line 507 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (3)].ast);															}
    break;

  case 87:

/* Line 1806 of yacc.c  */
#line 508 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_INVERSAO, 		NULL, NULL, 1, (yyvsp[(2) - (2)].ast));				}
    break;

  case 88:

/* Line 1806 of yacc.c  */
#line 509 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);															}
    break;

  case 89:

/* Line 1806 of yacc.c  */
#line 510 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);															}
    break;

  case 90:

/* Line 1806 of yacc.c  */
#line 514 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_SOMA, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 91:

/* Line 1806 of yacc.c  */
#line 515 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_SUBTRACAO, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 92:

/* Line 1806 of yacc.c  */
#line 516 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_MULTIPLICACAO, 	NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 93:

/* Line 1806 of yacc.c  */
#line 517 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_DIVISAO, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 94:

/* Line 1806 of yacc.c  */
#line 518 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_IGUAL, 	NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 95:

/* Line 1806 of yacc.c  */
#line 519 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_DIF, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 96:

/* Line 1806 of yacc.c  */
#line 520 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_E, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 97:

/* Line 1806 of yacc.c  */
#line 521 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_OU, 			NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 98:

/* Line 1806 of yacc.c  */
#line 522 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_L, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 99:

/* Line 1806 of yacc.c  */
#line 523 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_G, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 100:

/* Line 1806 of yacc.c  */
#line 524 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_LE, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 101:

/* Line 1806 of yacc.c  */
#line 525 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_GE, 		NULL, NULL, 2, (yyvsp[(1) - (3)].ast), (yyvsp[(3) - (3)].ast));				}
    break;

  case 102:

/* Line 1806 of yacc.c  */
#line 526 "parser.y"
    {verifyID(contextDict, (yyvsp[(1) - (1)].symbol)->key); (yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 103:

/* Line 1806 of yacc.c  */
#line 527 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 104:

/* Line 1806 of yacc.c  */
#line 528 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 105:

/* Line 1806 of yacc.c  */
#line 529 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 106:

/* Line 1806 of yacc.c  */
#line 530 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->key, 0);					}
    break;

  case 107:

/* Line 1806 of yacc.c  */
#line 531 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), charToString((yyvsp[(1) - (1)].symbol)->charValue), 0);	}
    break;

  case 108:

/* Line 1806 of yacc.c  */
#line 532 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_IDENTIFICADOR, 		(yyvsp[(1) - (1)].symbol), (yyvsp[(1) - (1)].symbol)->stringValue, 0);			}
    break;

  case 109:

/* Line 1806 of yacc.c  */
#line 533 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_LOGICO_COMP_NEGACAO, 	NULL, NULL, 1, (yyvsp[(2) - (2)].ast));				}
    break;

  case 110:

/* Line 1806 of yacc.c  */
#line 534 "parser.y"
    {(yyval.ast) = (yyvsp[(2) - (3)].ast);															}
    break;

  case 111:

/* Line 1806 of yacc.c  */
#line 535 "parser.y"
    {(yyval.ast) = create_ast_node(IKS_AST_ARIM_INVERSAO, 		NULL, NULL, 1, (yyvsp[(2) - (2)].ast));				}
    break;

  case 112:

/* Line 1806 of yacc.c  */
#line 536 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);															}
    break;

  case 113:

/* Line 1806 of yacc.c  */
#line 537 "parser.y"
    {(yyval.ast) = (yyvsp[(1) - (1)].ast);															}
    break;



/* Line 1806 of yacc.c  */
#line 2623 "/home/alanwink/compilers/compilerproject/testscript/g1/parser.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 2067 of yacc.c  */
#line 539 "parser.y"

