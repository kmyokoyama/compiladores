#!/bin/bash

# Script for running all tests for compilers project


# Create directory for binary
if [ ! -d g1 ]
 then
	mkdir g1
fi

cd g1/

# Cmake run
cmake ../..

if [ $? != 0 ]
 then
	echo Erro no CMAKE
	exit
fi

# Make run
make

if [ $? != 0 ]
 then
	echo Erro no MAKE
	exit
fi

cd ..

#Run professor's Script

./testall.sh g1