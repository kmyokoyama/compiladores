%x comment
%{

#include <stdlib.h>
#include "comp_dict.h"
#include "comp_tree.h"

#include "parser.h"

#define DICTIONARY_HASH_SIZE 1000

int lineNumber = 1; /*<! Line number control */

COMP_DICT_T *scannerDict; /*<! Dictionary pointer */

/** Dictionary init */
void init_dict(void) { 
	scannerDict = create_dictionary( DICTIONARY_HASH_SIZE, NULL);
}

/** Return line number where FLEX is. */
int getLineNumber(void) { 
	return lineNumber; 
}

%}

SPECIAL					[,;:(){}\[\]\+\-\*<>=!&$\/]				
INT						[0-9]							
CHAR						[a-zA-Z_]						

%%

\n 						{ lineNumber++;		}		/* For line number counter */

"int" 					{ return TK_PR_INT; 	}		/* Reserved words */
"float"					{ return TK_PR_FLOAT;	}
"bool" 					{ return TK_PR_BOOL; 	}
"char" 					{ return TK_PR_CHAR; 	}
"string" 					{ return TK_PR_STRING; 	}
"if" 						{ return TK_PR_IF; 	}
"then" 					{ return TK_PR_THEN; 	}
"else" 					{ return TK_PR_ELSE; 	}
"while" 					{ return TK_PR_WHILE; 	}
"do" 						{ return TK_PR_DO; 	}
"input" 					{ return TK_PR_INPUT; 	}
"output" 					{ return TK_PR_OUTPUT; 	}
"return" 					{ return TK_PR_RETURN; 	}

"false"					{ 	/* Boolean false value - add to dictionary*/
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_FALSE, getLineNumber()); 
							change_dictionary_item_bool(scannerDict, yytext, "false");
							return TK_LIT_FALSE; 
						}
							
"true"					{	/* Boolean true value - add to dictionary*/
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_TRUE, getLineNumber());
							change_dictionary_item_bool(scannerDict, yytext, "true");
							return TK_LIT_TRUE; 
						}

{SPECIAL} 					{ return yytext[0]; }			/* Return its character */

"<=" 						{ return TK_OC_LE; }			/* Composite characters */
">="			 			{ return TK_OC_GE; }
"==" 						{ return TK_OC_EQ; }
"!=" 						{ return TK_OC_NE; }
"&&" 						{ return TK_OC_AND; }
"||" 						{ return TK_OC_OR; }

{CHAR}({CHAR}|{INT})*			{ 	/* Identifier - add to dictionary */
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_IDENTIFICADOR, getLineNumber());
							return TK_IDENTIFICADOR;
						}		

{INT}{INT}*					{ 	/* Integer value - add to dictionary */
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_INT, getLineNumber());
							change_dictionary_item_int(scannerDict, yytext, yytext);
							return TK_LIT_INT; 
						}			

{INT}{INT}*\.{INT}{INT}*		{	/* Float value - add to dictionary */
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_FLOAT, getLineNumber());
							change_dictionary_item_float(scannerDict, yytext, yytext);
							return TK_LIT_FLOAT;
						}			

['][^']*[']					{ 	/* Char type between single quotes - add to dictionary */
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_CHAR, getLineNumber());
							change_dictionary_item_char(scannerDict, yytext, yytext);
							return TK_LIT_CHAR; 
						}			


["][^"]*["]					{ 	/* String type between double quotes - add to dictionary */
							yylval.symbol = insert_dictionary_item(scannerDict, yytext, TK_LIT_STRING, getLineNumber());
							change_dictionary_item_string(scannerDict, yytext, yytext);
							return TK_LIT_STRING; 
						}		


[ \t\v\f ]					{}				/* Tabulation (\t), Vertical tab (\v) and Formfeed (\f) => Ignore */		


\/\/.*					{}				/* Single line comments => Ignore */		


"/*"        				BEGIN(comment);		/* Multiple line comments => Ignore */
<comment>[^*\n]*
<comment>"*"+[^*/\n]*
<comment>\n             		lineNumber++;
<comment>"*"+"/"	        		BEGIN(INITIAL);


. 						{ return TOKEN_ERRO; }	/* Any other lexem is an error */

%%

