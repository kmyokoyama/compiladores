%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "comp_list.h"
#include "comp_dict.h"
#include "comp_tree.h"
#include "iks_ast.h"

#define DICTIONARY_HASH_SIZE 1000

COMP_TREE_T* root; /*< Root node for AST */

extern	COMP_DICT_T *scannerDict; 	/*< Scanner Dictionary reference 									*/
		COMP_DICT_T *globalDict; 	/*< Global Dictionary 											*/
		COMP_DICT_T *localDict; 	/*< Local Dictionary 											*/
		COMP_DICT_T *contextDict; 	/*< Context. Switch between local and global accordingly to the declaration type 	*/

COMP_DICT_ITEM_T* functionItem;

COMP_LIST_T *listOfParameters;

/**
 * Init global dict and the context
 */
 void initDict(){
 	globalDict = create_dictionary( DICTIONARY_HASH_SIZE, NULL); 
 	localDict = NULL;
 	contextDict = globalDict;
 }

/**
 *	Switch for local variable context.
 */
 void chageToLocalContext(){
 	
 	//printf("\n \n GLOBAL: \n");printDict(globalDict);
 	/* Create a new dictionary */
 	localDict = create_dictionary( DICTIONARY_HASH_SIZE, globalDict);

 	/* Change context */
 	contextDict = localDict;
 }

 /**
  * 	Switch for global context.
  */
void changeToGlobalContext(){

	//printf("\n \n LOCAL: \n");printDict(localDict);
	/* Change context */
	contextDict = globalDict;
}

/**
 * Convert a char to string
 */

char* charToString(char target){
	char* finalString;
	
	finalString = malloc(sizeof(char)*2);
	finalString[0] = target;
	finalString[1] = '\0';

	return finalString;	
}

/**
 * Create a list item for function analisys.
 */
COMP_LIST_T* functionListAdd(COMP_LIST_T* list, char* key, int itemType){
	COMP_DICT_ITEM_T* newDictNode;
	newDictNode = malloc(sizeof(COMP_DICT_ITEM_T));

	newDictNode->key = strdup(key);
	newDictNode->itemType = itemType;
	newDictNode->isVector = 0;
	newDictNode->isFunction = 0;
	newDictNode->ready = 1;

	return insert_last(list,  newDictNode); /* As the list is created backwards, always add the last node in the first place to reorder the list. */

}

%}

%union
{
	COMP_DICT_ITEM_T *symbol;
	COMP_TREE_T* ast;
	COMP_LIST_T* list;
	int value;
};

/* Reserved words */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN

/* Compound operators */
%token TK_OC_LE    		/* <=        */
%token TK_OC_GE    		/* >=        */
%token TK_OC_EQ    		/* ==        */
%token TK_OC_NE    		/* !=        */
%token TK_OC_AND   		/* &&        */
%token TK_OC_OR    		/* ||        */

/* Literals */
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING

/* Indentifier */
%token<symbol> TK_IDENTIFICADOR

/* Error */
%token TOKEN_ERRO



/* Type definitions */
%type<value> typeDeclaration

%type<symbol> TK_LIT_INT
%type<symbol> TK_LIT_FLOAT
%type<symbol> TK_LIT_CHAR
%type<symbol> TK_LIT_STRING
%type<symbol> TK_LIT_TRUE
%type<symbol> TK_LIT_FALSE

%type<ast> iksProgram
%type<ast> iksBegin
%type<ast> globalDeclaration
%type<ast> functionDeclaration
%type<ast> functionBody
%type<ast> functionBodyCommandList
%type<ast> functionHeader

%type<list>functionHeaderList
%type<list>functionHeaderListItemFirst
%type<list>functionHeaderListItemRest


%type<ast> functionVariables
%type<ast> functionCommand
%type<ast> functionCommandNext
%type<ast> functionCommandAttribution
%type<ast> functionCommandFlowControl
%type<ast> functionCommandFunctionCall
%type<ast> functionCommandInput
%type<ast> functionCommandOutput 
%type<ast> functionCommandReturn
%type<ast> vectorIndexing
%type<ast> functionCommandNoEmpty
%type<ast> expression
%type<ast> ifCommand
%type<ast> ifThenElseCommand
%type<ast> whileDoCommand
%type<ast> doWhileCommand 
%type<ast> functionCommandOutputListFirst
%type<ast> functionCommandOutputListRest
%type<ast> functionCommandFunctionCallParameterListFirst
%type<ast> functionCommandFunctionCallParameterListRest
%type<ast> expressionCompound

/* Precedence */
%right '='
%right TK_OC_OR
%right TK_OC_AND
%nonassoc '!' '<' '>' TK_OC_EQ TK_OC_NE TK_OC_LE TK_OC_GE
%left '+' '-'
%left '*' '/'
%right TK_PR_THEN
%right TK_PR_ELSE

%%
/* Rules and actions for IKS language grammar */

/* An IKS program is composed by global variable declarations and functions (both optional).  */
iksProgram
	: iksBegin globalDeclaration {root = create_ast_node(IKS_AST_PROGRAMA, NULL, NULL, 1, $2);} 
	;

iksBegin
	:	{initDict();}	/** Init dict and context before anything */
	;

globalDeclaration 
	: variableDeclaration globalDeclaration		{$$ = $2;							}
	| functionDeclaration globalDeclaration		{$$ = add_son_ast_node($1, 1, $2);			}
	|								{$$ = NULL;							}
	;

/* All global variable declarations are terminated by ';' */
variableDeclaration
	: typeDeclaration ':' TK_IDENTIFICADOR ';'		{ 	change_dictionary_item_itemValue(scannerDict, $3->key, $1);
														verifyDeclaration(contextDict, $3->key);
														COMP_DICT_ITEM_T* newItem;
														newItem = insert_dictionary_item(contextDict, $3->key, $1, $3->lineNumber);
														newItem->parameterList = NULL;
														newItem->isVector = 0;
														newItem->isFunction = 0;
														newItem->ready = 1;
													setSizeDict(newItem); }
	| typeDeclaration ':' TK_IDENTIFICADOR '[' TK_LIT_INT ']' ';'	
												{ 	change_dictionary_item_itemValue(scannerDict, $3->key, $1);
													verifyDeclaration(contextDict, $3->key);
													COMP_DICT_ITEM_T* newItem;
													newItem = insert_dictionary_item(contextDict, $3->key, $1, $3->lineNumber);
													newItem->parameterList = NULL;
													newItem->isVector = 1;
													newItem->isFunction = 0;
													newItem->ready = 1;
												}

/* Types in declarations */
typeDeclaration
	: TK_PR_INT 	{$$ = IKS_SIMBOLO_INT; 		}
	| TK_PR_FLOAT	{$$ = IKS_SIMBOLO_FLOAT; 	}
	| TK_PR_CHAR	{$$ = IKS_SIMBOLO_CHAR; 	}
	| TK_PR_BOOL	{$$ = IKS_SIMBOLO_BOOL; 	}
	| TK_PR_STRING	{$$ = IKS_SIMBOLO_STRING; 	}
	;
	
/* Each function has a header and a body */
functionDeclaration
	:   functionHeader functionVariables functionBody {$$ = add_son_ast_node($1, 1, $3);changeToGlobalContext();/* End of function */}  


/* The function header has a return value followed by ':', function name and a list of parameters */
functionHeader
	: 	typeDeclaration ':' TK_IDENTIFICADOR 
			{
				verifyDeclaration(contextDict, $3->key);
				
				insert_dictionary_item(contextDict, $3->key, $1, $3->lineNumber);
				change_dictionary_item_itemValue(scannerDict, $3->key, $1);
				
				functionItem = get_dictionary_item(contextDict, $3->key);
				functionItem->isVector = 0;
				functionItem->isFunction = 1;
				functionItem->ready = 0;
				
				
				chageToLocalContext();
			}
		functionHeaderList 	
			{	
				functionItem = get_dictionary_item(globalDict, $3->key); /* Function declaration is in global */
				functionItem->parameterList = $5;
				functionItem->ready = 1;
				$$ = create_ast_node(IKS_AST_FUNCAO, NULL, $3->key, 0);
			}
	;

/* The function header list is a list of input parameters between '(' and ')' (could be empty) */
functionHeaderList
	: '(' functionHeaderListItemFirst ')'					{$$ = $2;}

/* Each parameter is separated by ',' and is defined by type, ':' and its name */
functionHeaderListItemFirst
	: typeDeclaration ':' TK_IDENTIFICADOR functionHeaderListItemRest{ 	
													change_dictionary_item_itemValue(scannerDict, $3->key, $1);
													verifyDeclaration(contextDict, $3->key);
													insert_dictionary_item(contextDict, $3->key, $3->itemType, $3->lineNumber);
													change_dictionary_item_itemValue(contextDict, $3->key, $1);
													$$ = functionListAdd($4, $3->key, $1); /* Create a list item for functions */
												}
	|											{	$$ = NULL;	}
	;

functionHeaderListItemRest
	: ',' typeDeclaration ':' TK_IDENTIFICADOR functionHeaderListItemRest{ 	
												 		change_dictionary_item_itemValue(scannerDict, $4->key, $2);
												 		verifyDeclaration(contextDict, $4->key);
												 		insert_dictionary_item(contextDict, $4->key, $4->itemType, $4->lineNumber);
														change_dictionary_item_itemValue(contextDict, $4->key, $2);
														$$ = functionListAdd($5, $4->key, $2); /* Create a list item for functions */
													}
	|												{ $$ = NULL;	}
	;

/* The local declarations cannot accept vectors */
functionVariables
	: typeDeclaration ':' TK_IDENTIFICADOR ';' functionVariables{
													verifyDeclaration(contextDict, $3->key);
													change_dictionary_item_itemValue(scannerDict, $3->key, $1);
													COMP_DICT_ITEM_T* newItem;
													newItem = insert_dictionary_item(contextDict, $3->key, $1, $3->lineNumber);
													newItem->parameterList = NULL;
													newItem->isVector = 0;
													newItem->isFunction = 0;
													newItem->ready = 1;
												}
	|
	;

/* The function body is a list of commands between '{' and '}' */
functionBody
	: '{' functionBodyCommandList '}' {$$ = $2; }
	;

/* The command list could be empty, another command list, a single command or a list of commands separated by ';' */
functionBodyCommandList
	: functionCommand functionCommandNext { $$ = add_son_ast_node($1, 1, $2); }
	;

/* If it has next, it is separated by ';', or the last one has ';' */
functionCommandNext
	: ';' functionBodyCommandList 	{$$ = $2;	}
	|						{$$ = NULL;	}
	;

/* Commands can be a command block, attribution, flow control, function call or "input", "output", "return" or empty commands */	
functionCommand
	: functionBody 							{$$ = create_ast_node(IKS_AST_BLOCO, NULL, NULL, 1, $1);	}
	| functionCommandAttribution					{$$ = $1;									}
	| functionCommandFlowControl					{$$ = $1;									}
	| functionCommandFunctionCall					{$$ = $1;									}
	| functionCommandInput						{$$ = $1;									}
	| functionCommandOutput 					{$$ = $1;									}
	| functionCommandReturn 					{$$ = $1;									}	
	|									{$$ = NULL;									}
	;

/* Blocks that requires a command*/
functionCommandNoEmpty
	: functionBody 							{$$ = create_ast_node(IKS_AST_BLOCO, NULL, NULL, 1, $1);	}
	| functionCommandAttribution					{$$ = $1;									}
	| functionCommandFlowControl					{$$ = $1;									}
	| functionCommandFunctionCall					{$$ = $1;									}
	| functionCommandInput						{$$ = $1;									}
	| functionCommandOutput 					{$$ = $1;									}
	| functionCommandReturn 					{$$ = $1;									}
	;

/* Attribution command */
functionCommandAttribution
	: TK_IDENTIFICADOR '=' expression 	{	verifyID(contextDict, $1->key);
											verifyVariable(contextDict, $1->key);
											$$ = create_ast_node(
												IKS_AST_ATRIBUICAO,
												NULL,
												NULL, 
												2,	
												create_ast_node(IKS_AST_IDENTIFICADOR, $1, $1->key, 0),
												$3
								     			); 
							                    verifyAttribution($$);}
	| vectorIndexing '=' expression 	{ 
								$$ = create_ast_node( IKS_AST_ATRIBUICAO, NULL, NULL, 2, $1, $3);	
							                  verifyAttributionVector($$);}
	;

/* Vector indexing operation */
vectorIndexing
	: TK_IDENTIFICADOR '[' expression ']' {
								verifyVector(contextDict, $1->key);
								$$ = create_ast_node(
										IKS_AST_VETOR_INDEXADO,
										NULL,
										NULL, 
										2,	
										create_ast_node(IKS_AST_IDENTIFICADOR, $1, $1->key, 0),
										$3
									); 
							}
	;

/* Function command flow types*/
functionCommandFlowControl
	: ifCommand 					{$$ = $1;}
	| ifThenElseCommand 			{$$ = $1;}
	| whileDoCommand 				{$$ = $1;}
	| doWhileCommand 				{$$ = $1;}
	;

ifCommand
	: TK_PR_IF '(' expression ')' TK_PR_THEN functionCommandNoEmpty
		{
			$$ = create_ast_node(IKS_AST_IF_ELSE, NULL, NULL, 2, $3, $6);
		}
	;

ifThenElseCommand
	: TK_PR_IF '(' expression ')' TK_PR_THEN functionCommandNoEmpty TK_PR_ELSE functionCommandNoEmpty 		
		{
			$$ = create_ast_node(IKS_AST_IF_ELSE, NULL, NULL, 3, $3, $6, $8);
		}
	;

whileDoCommand
	: TK_PR_WHILE '(' expression ')' TK_PR_DO functionCommandNoEmpty 		
		{
			$$ = create_ast_node(IKS_AST_WHILE_DO, NULL, NULL, 2, $3, $6);
		}
	;

doWhileCommand
	: TK_PR_DO functionCommandNoEmpty TK_PR_WHILE '(' expression ')' 		
		{
			$$ = create_ast_node(IKS_AST_DO_WHILE , NULL, NULL, 2, $2, $5);
		}
	;

functionCommandFunctionCall
	: TK_IDENTIFICADOR '(' functionCommandFunctionCallParameterListFirst ')'	
		{
			verifyFunction(contextDict, $1->key);
			$$ = create_ast_node(
						IKS_AST_CHAMADA_DE_FUNCAO ,
						NULL, 
						NULL, 
						2, 
						create_ast_node(IKS_AST_IDENTIFICADOR, $1, $1->key, 0), 
						$3
					);
			verifyParameters(contextDict, $1->key, listOfParameters);
			/*remove_list(listOfParameters);*/
		}
	;

functionCommandFunctionCallParameterListFirst
	: expression functionCommandFunctionCallParameterListRest		{$$ = add_son_ast_node($1, 1, $2);
																	listOfParameters = insert_last(listOfParameters, $1);}
	|											{$$ = NULL;					}
	;

functionCommandFunctionCallParameterListRest
	: ',' expression functionCommandFunctionCallParameterListRest	{$$ = add_son_ast_node($2, 1, $3);
																	listOfParameters = insert_last(listOfParameters, $2);}
	|											{$$ = NULL;					}
	;

/* Input command has a "input" followed by an identifier */
functionCommandInput
	: TK_PR_INPUT expression 					
		{
			$$ = create_ast_node(
						IKS_AST_INPUT , 
						NULL, 
						NULL, 
						1, 
						$2
					);
		}
	;
	
/* An output command has a list of elements separated by ',' that can be a string or an arithmetic expression */
functionCommandOutput
	: TK_PR_OUTPUT functionCommandOutputListFirst 		{$$ = create_ast_node(IKS_AST_OUTPUT, NULL, NULL,1,  $2);}
	;

functionCommandOutputListFirst
	: expression functionCommandOutputListRest		{$$ = add_son_ast_node($1, 1, $2);	}
	| expression 							{$$ = $1; 					}
	;

functionCommandOutputListRest
	: ',' expression functionCommandOutputListRest		{$$ = add_son_ast_node($2, 1, $3);	}
	| ',' expression 							{$$ = $2; 					}
	;	

/* Return command has a "return" followed by an expression */
functionCommandReturn
	: TK_PR_RETURN expression 					{$$ = create_ast_node( IKS_AST_RETURN , NULL, NULL, 1,$2);
													verifyReturn(functionItem, $2);
												}
	;
	
/* Expressions in general */	
expression
	: expressionCompound '+' expressionCompound		{$$ = create_ast_node(IKS_AST_ARIM_SOMA, 			NULL, NULL, 2, $1, $3);	
								                    verifyArithOperation($$);									        }		
	| expressionCompound '-' expressionCompound 		{$$ = create_ast_node(IKS_AST_ARIM_SUBTRACAO, 		NULL, NULL, 2, $1, $3);				
								                    verifyArithOperation($$);	                                        }
	| expressionCompound '*' expressionCompound 		{$$ = create_ast_node(IKS_AST_ARIM_MULTIPLICACAO, 	NULL, NULL, 2, $1, $3);				
								                    verifyArithOperation($$);                                           }
	| expressionCompound '/' expressionCompound		{$$ = create_ast_node(IKS_AST_ARIM_DIVISAO, 		NULL, NULL, 2, $1, $3);				
								                    verifyArithOperation($$);                                           }
	| expressionCompound TK_OC_EQ expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_IGUAL, 	NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_NE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_DIF, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_AND expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_E, 			NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_OR expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_OU, 			NULL, NULL, 2, $1, $3);				}
	| expressionCompound '<' expressionCompound		{$$ = create_ast_node(IKS_AST_LOGICO_COMP_L, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound '>' expressionCompound		{$$ = create_ast_node(IKS_AST_LOGICO_COMP_G, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_LE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_LE, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_GE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_GE, 		NULL, NULL, 2, $1, $3);				}
	| TK_IDENTIFICADOR 						{verifyID(contextDict, $1->key); $$ = create_ast_node(IKS_AST_IDENTIFICADOR, $1, $1->key, 0);}
	| TK_LIT_TRUE							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_FALSE							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_INT							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_FLOAT							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_CHAR							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, charToString($1->charValue), 0);	}
	| TK_LIT_STRING 							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->stringValue, 0);			}
	| '!' expressionCompound					{$$ = create_ast_node(IKS_AST_LOGICO_COMP_NEGACAO, 	NULL, NULL, 1, $2);				}
	| '(' expressionCompound ')' 					{$$ = $2;															}
	| '-' expressionCompound 					{$$ = create_ast_node(IKS_AST_ARIM_INVERSAO, 		NULL, NULL, 1, $2);				}
	| vectorIndexing 							{$$ = $1;															}
	| functionCommandFunctionCall 				{$$ = $1;															}
	;

expressionCompound
	: expressionCompound '+' expressionCompound		{$$ = create_ast_node(IKS_AST_ARIM_SOMA, 			NULL, NULL, 2, $1,$3);	 
								                     verifyArithOperation($$);                                                                }
	| expressionCompound '-' expressionCompound 		{$$ = create_ast_node(IKS_AST_ARIM_SUBTRACAO, 		NULL, NULL, 2, $1, $3);				
													  verifyArithOperation($$);	                                                              }
	| expressionCompound '*' expressionCompound 		{$$ = create_ast_node(IKS_AST_ARIM_MULTIPLICACAO, 	NULL, NULL, 2, $1, $3);				
													  verifyArithOperation($$);                                                               }
	| expressionCompound '/' expressionCompound		{$$ = create_ast_node(IKS_AST_ARIM_DIVISAO, 		NULL, NULL, 2, $1, $3);				
								                      verifyArithOperation($$);                                                               }
	| expressionCompound TK_OC_EQ expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_IGUAL, 	NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_NE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_DIF, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_AND expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_E, 			NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_OR expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_OU, 			NULL, NULL, 2, $1, $3);				}
	| expressionCompound '<' expressionCompound		{$$ = create_ast_node(IKS_AST_LOGICO_COMP_L, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound '>' expressionCompound		{$$ = create_ast_node(IKS_AST_LOGICO_COMP_G, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_LE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_LE, 		NULL, NULL, 2, $1, $3);				}
	| expressionCompound TK_OC_GE expressionCompound	{$$ = create_ast_node(IKS_AST_LOGICO_COMP_GE, 		NULL, NULL, 2, $1, $3);				}
	| TK_IDENTIFICADOR 						{verifyID(contextDict, $1->key); $$ = create_ast_node(IKS_AST_IDENTIFICADOR, $1, $1->key, 0);}
	| TK_LIT_TRUE							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_FALSE							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_INT							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_FLOAT							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->key, 0);					}
	| TK_LIT_CHAR							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, charToString($1->charValue), 0);	}
	| TK_LIT_STRING 							{$$ = create_ast_node(IKS_AST_IDENTIFICADOR, 		$1, $1->stringValue, 0);			}
	| '!' expressionCompound					{$$ = create_ast_node(IKS_AST_LOGICO_COMP_NEGACAO, 	NULL, NULL, 1, $2);				}
	| '(' expressionCompound ')' 					{$$ = $2;															}
	| '-' expressionCompound 					{$$ = create_ast_node(IKS_AST_ARIM_INVERSAO, 		NULL, NULL, 1, $2);				}
	| vectorIndexing 							{$$ = $1;															}
	| functionCommandFunctionCall 				{$$ = $1;															}
	;
%%